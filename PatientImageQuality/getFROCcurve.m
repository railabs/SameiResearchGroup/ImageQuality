% Copyright 20xx - 2019. Duke University
function [TPR, NFP,varargout] = getFROCcurve(class,clicked,score,nCases,varargin)
%Calculate an FROC curve from free response reader data
%   [TPR, NFP] = getFROCcurve(class,detected,score,nCases) calculates the
%   free-response ROC curve (FROC). class is a nResponses x 1 binary array
%   of responses which defines the true state of each response (e.g.
%   class(i) == true means that the ith response corresponds to a case with
%   a lesion). clicked is a nResponses x 1 binary array which says if the
%   reader clicked (or marked) the ith response (e.g., clicked(i) == true
%   means the user clicked on the ith lesion). score is a confidence score
%   corresponding to each response. nCases is the total number of cases
%   represented by the responses (used to calculate NFP) TPR is the true
%   positive rate (sensitivity) and NFP is the average number of false
%   positives per case.


%Get the settings
[thresholds,nBoot] = parseInputs(varargin,score);


switch nargout
    case 2
        %This is run when the user doesn't want variability
        out = calculateFROC(class,clicked,score,nCases,thresholds);
        TPR = out(:,1); NFP = out(:,2);
    case 3
        %This estimates variability using bootstrapping (point estimate is
        %middle point between confidence intervals
        bootfun = @(classi,clickedi,scorei) calculateFROC(classi,clickedi,scorei,nCases,thresholds);
        options =  statset;
        CI = bootci(nBoot,{bootfun,class,clicked,score},'Options',options);
        stats.TPR_CI = CI(:,:,1)';
        stats.NFP_CI = CI(:,:,2)';
        varargout{1} = stats;
        
        TPR = mean(stats.TPR_CI,2);
        NFP = mean(stats.NFP_CI,2);
end



end

function [thresholds,nBoot] = parseInputs(args,score)
%This function parses all the optional inputs

%Default settings
thresholds = unique(score);
nBoot = 500; %Number of bootstrap samples when estimating uncertainty

%loop through extra arguments and grab the values
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'Thresholds'
                thresholds = args{i+1};
            case 'nBoot'
                nBoot = args{i+1};
        end
    end
    
end

end

function out = calculateFROC(class,clicked,score,nCases,thresholds)

%Get the total number of positive cases
nPositives = sum(class);

for i=1:length(thresholds)
    %get the threshold
    t = thresholds(i);
    
    %Calculate sensitivity
    ind = class & clicked & score>=t;
    TPR(i,1) = sum(ind) / nPositives;
    
    %Calculate average number of false positives
    ind = ~class & clicked & score>=t;
    NFP(i,1) = sum(ind)/nCases;
    
end

out = [TPR,NFP];

end