function [noise,count]=getGlobalNoiseIndexForImquest(dcmimg,range,rsize)

if nargin==1
    range=[];
    rsize=[];
elseif nargin==2
    rsize=[];
    LOG_filename=[];
end

try 
    
    count=[];
    noise=NaN;
    
    if isempty(range)
        range=[-300 100];
    end
    
    if isempty(rsize)
        rsize=9;
    end
    
    rsize=floor(rsize/2)*2+1;
    
    h=fspecial('average',rsize);
    avg=filter2(h,dcmimg);
    
    vars=stdfilt(dcmimg,ones(rsize));
    vars(1:floor(rsize/2),:)=NaN;
    vars(size(vars,1)-floor(rsize/2):size(vars,1),:)=NaN;
    vars(:,1:floor(rsize/2))=NaN;
    vars(:,size(vars,2)-floor(rsize/2):size(vars,2))=NaN;
    vars(avg<range(1) | avg>range(2))=NaN;
    
    t=0.1;
    
    [h,g]=hist(vars(vars<200),(0:t:200));
    h(1)=0;
    h=smooth(h,round(2/t));
    
    % JMW 11/23/2014
    i=find(h>0.90*max(h));
    
    if length(i)>1
        q=g(i);
        if 1<(length(q)-1)
            q=q(1:end-1);
        elseif 1==(length(q)-1)
            q=q(1);
        else
            q=double.empty(1,0); %NaN JMW 5/3/2017
        end
        %%%%%%%%%%%%%%%%%%
        
        d=diff(g(i))./q;
        gap=find(d>0.1,1,'first');
        %gap=min(find(d>0.1));
        if gap
            i(gap+1:end)=[];
        end
        
        g=g(:);
        h=h(:);
        noise=mean(g(i).*h(i))/mean(h(i));
        count=length(i);
    end
    
catch PROBLEM
    warning(PROBLEM)
end
end
