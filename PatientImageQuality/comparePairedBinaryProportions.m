% Copyright 20xx - 2019. Duke University
function results = comparePairedBinaryProportions(data,alpha)
%This function performs McNemar's test to statistically compare paired binary data
%data is a nx2 column vector of binary responses (0 or 1). Each column of
%data contains the outcomes of binary trial for the condition corresponding
%to that trial. For example, say you're comparing two recons using an
%alternative forced choice experiment and you show the same cases to
%readers, reconstructed in two ways (i.e., FBP and IR). 

%Create contingency table
data=logical(data);
s=sum(~data(:,1) & data(:,2)); %number of cases where the first condition was false but the second condition was true
r=sum(data(:,1) & ~data(:,2)); %number of cases where the first condition was true but the second condition was false

%compute chi squared test statistic
chisqr = ((abs(r-s)-1)^2)/(r+s);

%Get p value
p=1-chi2cdf(chisqr,1);

%compare p value to significance threshold
if p<=alpha
    significant = true;
else
    significant = false;
end

%compute difference between classes
N=size(data,1);
diff = (s-r)/N;

%compute standard error of difference
SE=sqrt(r+s)/N;

%compute 1-alpha confidence intervals
mult =norminv(1-(alpha/2),0,1);
CI = [diff-mult*SE diff+mult*SE];

%Compute odds ratio
OR = s/r;

%compute odds ratio confidence interval
chi = sqrt(chisqr);
OR_CI=[OR^(1-(mult/chi)) OR^(1+(mult/chi))];

results.prop1=mean(data(:,1));
results.prop2=mean(data(:,2));
results.diff=diff;
results.diff_CI=CI;
results.significant=significant;
results.pvalue=p;
results.alpha=alpha;
results.ChiSquared=chisqr;
results.StandardError=SE;
results.OddsRatio=OR;
results.OddsRatio_CI=OR_CI;
results.nSamples=N;





end