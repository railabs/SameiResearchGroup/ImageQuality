This is a large repository of codes used to compute image quality (IQ) metrics. It has 5 subdirectories as described below.

1. IQmodelo is a package written by Adam Wunderlich of the FDA, its used to compute IQ metrics, and specifically good for getting confidence intervals for a channelized Hoteling observer.

2. NPS contains code to make NPS calculations, both using the traditional methods with square ROIs, and using a new methods based on arbirtrarily shaped ROIs (see getAutoCovariance2D).

3. ObserverModels has code to compute d' for several different observers (CHO and NPW families).

4. PatientImageQuality has code to measure noise in patient images.

5. TTF contains code to get in-plane and z-direction TTFs.

Written by Justin Solomon
10/23/15 
