% Copyright 20xx - 2019. Duke University
function Gmag = getAverageRelativeGradientMagnitude(im,varargin)
%This function returns the average relative gradient magnitude from an
%image.
%
%   Gmag = getAverageRelativeGradientMagnitude(im) uses defualt settings.
%   If im is 3D, then Gmag is a vector corresponding to each slice,
%   otherwise Gmag is scalar.
%
%   Gmag = getAverageRelativeGradientMagnitude(im,name,value,...) lets the
%   user input non-defualt settings as name-value pairs of inputs.
%   
%Settings------------------------------------------------------------------
%   mask
%   binary logical array used to define an ROI in which to make the
%   gradient measurements. Default is to use the entire image.
%
%   normFactor 
%   scalar number used to normalize the gradient. Default is normFactor =
%   max(abs(im(mask))). For a stack of images, the same normalization
%   factor is used for all slices.
%
%   psize
%   physical size of the pixels. The default is 1. Gmag will be in units of
%   1/u where u is the units of psize (usually mm). 
%--------------------------------------------------------------------------

%Based on Solomon J, Marin D, Roy Choudhury K, Patel B, Samei E (2017)
%Effect of radiation dose reduction and reconstruction algorithm on image
%noise, contrast, resolution, and detectability of subtle hypoattenuating
%liver lesions at multidetector CT: filtered back projection versus a
%commercial model-based iterative reconstruction algorithm. Radiology
%284:777?787

%Get the settings
[mask,normFactor,psize] = parseInputs(im,varargin);

%Initialize Gmag
Gmag = zeros(size(im,3),1);

%Caluluate the norm factor
n = (1/psize)/normFactor;

%loop over each slice
for i=1:size(im,3)
    [gmag,~] = imgradient(im(:,:,i),'central'); %Get raw image gradient
    gmag=gmag*n; %Scale by the norm factor
    Gmag(i,1) = mean(gmag(mask(:,:,i))); %take average within image mask
end

end

function [mask,normFactor,psize] = parseInputs(im,args)
mask = true(size(im));
normFactor = max(abs(im(mask)));
psize = 1;

if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'mask'
                mask = args{i+1};
            case 'normFactor'
                normFactor = args{i+1};
            case 'psize'
                psize = args{i+1};
        end
    end
end

end