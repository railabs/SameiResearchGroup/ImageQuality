function [f, TTF, varargout] = get2DTTF(I, ROI, psize, varargin)
%This function measures the 2D Task Transfer Function (TTF) using the
%rod-based method.
%--------------------------------------------------------------------------
%Syntax:
%   [f, TTF] = get2DTTF(I,ROI,psize) computes the TTF for a stack of
%   images, I. ROI = [Cx Cy R rod] defines the center (Cx and Cy in pixels)
%   and radius (in mm) of the ROI (R) and of the rod. psize is the pixel
%   size.
%
%   [f, TTF, contrast] = get2DTTF(...) additionally returns the contrast of
%   the rod
%
%   [f, TTF, contrast, settings] = get2DTTF(...) returns a structured
%   variable of the settings used to compute the TTF.
%
%   [f, TTF, contrast, settings, stats] = get2DTTF(...) returns some
%   statistics about the TTF (f50, f10, CNRall, and CNRslice)
%
%   [...] = get2DTTF(...,name,value,...) uses optional settings to compute
%   the TTF.
%--------------------------------------------------------------------------
%Optional settings
%
%   CenterMode
%   'Slice' (default), 'Mean'
%   CenterMode controls if the center of the rod is found on each slice
%   individually or using a mean image (averaged across slices).
%
%   CenterMethod
%   'CrossCorrelation' (default), 'WeightedAverage'
%   CenterMethod controls the algorithm used to find the rod center. The
%   CrossCorrelation method finds the max of a cross correlation between
%   the image and a circle (with radius given by ROI(4)). The
%   WeightedAverage method sets the center of the rod to the wieghted
%   avearge of the image (within the ROI).
%
%   Scale
%   This value controls the size of the bins for the re-binning and
%   averaging step of the ESF (default = 10). The bin size is equal to
%   psize/Scale (i.e., the bigger the scale, the more finely the ESF is
%   sampled).
%
%   Window
%   This value controls the width of the Hann window applied to the PSF
%   (default = 15).
%
%   TTFMode
%   'Slice' (default), 'Mean'
%   Controls if the TTF is computed on each slice's ESF (Slice) or using an
%   ensemble ESF from all slices at once (Mean)
%
%   BKGMethod
%   'Poly' (default), 'Mean', 'None'
%   Controls the method by which the background trend is subtracted from
%   the ROI.
%
%   Tforeground 
%   This value (between 0 1) controls the relative size of the
%   foreground region used to compute the contrast of the rod. Default is
%   0.2.
%
%   Tbackground
%   This value (between 0 1) controls the relative size of the background
%   region used to compute the contrast of the rod. Default is 0.8.
%
%   TCNR
%   This value determines the CNR threshold at which the TTF is valid.
%   Default is 15 [based on Chen, B., Christianson, O., Wilson, J. M., &
%   Samei, E. (2014). Assessment of volumetric noise and resolution
%   performance for linear and nonlinear CT reconstruction methods. Medical
%   Physics, 41(7)]
%
%   Override
%   Boolean that controls if the chosen (or default) settings can be
%   overwritten in the case that the CNR is too low. If Override is true,
%   then the settings can be overwritten. This happens when the CNR in the
%   individual slices is less than TCNR. In this case, TTFMode is changed
%   to 'Mean'.
%
%   fRange
%   1x2 vector describing the desired range of the output frequencies.
%   Default is [0 2];
%
%   fSamples
%   Integar value that controls the length/spacing of the output f and TTF.
%   Default is 128.
%
%   Scale_CC
%   Controls the upsample rate for doing the cross-correlation to find the
%   center of the rod. Default is 4
%
%   Verbose
%   Boolean that controls if warnings are given during execution of code.
%   Default is true.
%
%   Condition
%   Boolean that controls if the binned ESF is conditioned. Default is
%   false but if the CNRall is less than TCNR (and Override is true), then
%   Condition will be changed to true.
%--------------------------------------------------------------------------
%Notes:
%   This code is a combination of codes written by Olav Christianson, Baiyu
%   Chen, Sam Richard, Jered Wells and Justin Solomon. It is based on the
%   paper cited below. This current implementation was written by Justin
%   Solomon
%
%   Richard, S., et al. (2012). Towards task-based assessment of CT
%   performance: system and object MTF across different reconstruction
%   algorithms. Med Phys, 39(7), 4115?4122.
%
%   Version 1.0 - 3/29/15
%   Version 1.1 - 3/31/15 Added the abilty to apply ESF conditioning
%   (smoothing) to be able to get a TTF for low CNR situations.
%   Version 1.2 - 4/1/15 Fixed a bug in the cross-correlation code that is
%   used to find the rod center.
%   Version 1.3 - 4/3/15 Changed the conditioning function to use code from
%   Jered Wells.
%
%   Send questions to justin.solomon@duke.edu
%--------------------------------------------------------------------------

%Parse the optional input parameters
[CenterMode, scale, window, CenterMethod, TTFMode, BKGMethod, Tforeground, Tbackground, TCNR, Override, fRange, fSamples, Scale_CC, Verbose, Condition] = parseInputs(varargin);

%Define a coordinate system
[X, Y]=meshgrid(1:size(I,2),1:size(I,1));
R=psize*sqrt((X-ROI(1)).^2 +(Y-ROI(2)).^2);
mask=R<=ROI(3);

%Get the bounding box and crop the image
stats = regionprops(mask,'BoundingBox');
box = round(stats.BoundingBox);
I = I(box(2):box(2)+box(4)-1,box(1):box(1)+box(3)-1,:);
R = R(box(2):box(2)+box(4)-1,box(1):box(1)+box(3)-1);
mask=R<=ROI(3);

%Re-define coordinate system for the cropped image
[X, Y]=meshgrid(1:size(I,2),1:size(I,1));

%Check the CNR to see if there is enough data to get a reliable TTF
[CNRall, CNRslice, noise, HUtarget, bkgTarget] = getCNR(I,mask,Tforeground,Tbackground);

%Warn user if overall CNR is too low. Turn on conditioning in this case
CNRtotalWarning = false;
ConditionChanged = false;
if CNRall < TCNR
    if Verbose
        warning(['Total CNR may be too low for a reliable TTF measurement. Total CNR = ' num2str(CNRall) ', Threshold = ' num2str(TCNR)]);
    end
    CNRtotalWarning = true;
    if Override
        if ~Condition
            Condition = true;
            ConditionChanged = true;
            if Verbose
                warning('Condition has been changed to true due to low total CNR');
            end
        end
    end
end

%Warn user and change settings if slice CNR is too low to measure TTF on
%each slice individually
CNRsliceWarning = false;
TTFModeChanged = false;
switch TTFMode
    case 'Slice'
        if CNRslice<TCNR
            if Verbose
                warning(['CNR in each slice may be too low for a reliable TTF measurement. Slice CNR = ' num2str(CNRslice) ', Threshold = ' num2str(TCNR)]);
            end
            CNRsliceWarning = true;
            if Override
                TTFMode = 'Mean';
                CenterMode = 'Mean';
                if Verbose
                    warning('TTFMode and CenterMode have been changed to ''Mean'' due to low slice CNR');
                end
                TTFModeChanged = true;
            end
        end
end

%Find the center of the insert (and compute the contrast)
switch CenterMode
    case 'Mean' %Finds the center using a single averaged image
        %Compute mean of image
        im = mean(I,3);
        
        %remove background using specified method
        im = subtractBackground(im,mask,BKGMethod,Tbackground);
        
        %Find the center using the specified method
        switch CenterMethod
            case 'WeightedAverage'
                [center, contrast] = findCenter(im,mask,Tforeground, Tbackground);
            case 'CrossCorrelation'
                [center,contrast] = findCenter_CC(im,Scale_CC,ROI(4)/psize,mask,Tforeground, Tbackground);
        end
        center = repmat(center,[size(I,3) 1]);
    case 'Slice' %Finds the center on each slice individually
        for i=1:size(I,3)
            %Get the slice of interest
            im = I(:,:,i);
            
            %remove background with polynomial fit
            im = subtractBackground(im,mask,BKGMethod,Tbackground);
            switch CenterMethod
                case 'WeightedAverage'
                    [center(i,:), contrast(i)] = findCenter(im,mask,Tforeground, Tbackground);
                case 'CrossCorrelation';
                    [center(i,:), contrast(i)] = findCenter_CC(im,Scale_CC,ROI(4)/psize,mask,Tforeground, Tbackground);
            end
        end
        contrast = mean(contrast);
end

%Compute the distance of each pixel from the center of the rod
clear R;
for i=1:size(I,3)
    Cx = center(i,1); Cy = center(i,2);
    R(:,:,i) = psize*sqrt((X-Cx).^2 +(Y-Cy).^2);
end

%Compute the TTF
bsize = psize/scale;
edges = 0:bsize:ROI(3);
ConditionError = false;
switch TTFMode
    case 'Mean' %Computes a single ensemble ESF to get the TTF using all slices at once
        %Get the distances (r) and pixel values (HU) of interest
        mask = repmat(mask,[1 1 size(I,3)]);
        
        %Subtract the background
        I = subtractBackground(I,mask,BKGMethod,Tbackground);
        r = R(mask);
        HU = I(mask);
        
        %bin and average the data
        [distance, ESF, variance] = rebinData(r,HU,edges);
        
        %Condition the ESF
        if Condition
            try
                %[ESF,distance] = esfcond_v6(ESF,distance,0,1./variance);
                %ESF=ESF'; distance=distance';
                ESF = esfcond(ESF)';
                ConditionError = false;
            catch
                ConditionError = true;
            end
        end
        
        %convert the ESF to the TTF
        [f, TTF] = ESF2TTF(distance,ESF,bsize,window);
    case 'Slice' %Computes an ESF and TTF for each individual slice and then takes the average
        for i=1:size(I,3)
            %Subtract the background
            im = I(:,:,i);
            im = subtractBackground(im,mask,BKGMethod,Tbackground);
            I(:,:,i)=im;
            
            %Get the distances (r) and pixel values (HU) of interest
            r = R(:,:,i); r=r(mask);
            HU=im(mask);
            
            %bin and average the data
            [distance, ESF, variance] = rebinData(r,HU,edges);
            
            %Condition the ESF
            if Condition
                try
                    %[ESF,distance] = esfcond_v6(ESF,distance,0,1./variance);
                    %ESF=ESF'; distance=distance';
                    ESF = esfcond(ESF)';
                    ConditionError(i) = false;
                catch
                    ConditionError(i) = true;
                end
            end
            
            %convert the ESF to the TTF
            [f, TTF(:,i)] = ESF2TTF(distance,ESF,bsize,window);
        end
        %remove TTFs that could not be conditioned
        if Condition
            TTF = TTF(:,~ConditionError);
        end
        TTF = mean(TTF,2);
end

%Resample the TTF according to fRange and fSamples
f_int = linspace(fRange(1),fRange(2),fSamples);
TTF = interp1(f,TTF,f_int,'linear','extrap')';
f = f_int';

%Set optional outputs
n = nargout-2;
for i=1:n
    switch i
        case 1
            varargout{i}=contrast;
        case 2
            settings.CenterMode = CenterMode;
            settings.CenterMethod = CenterMethod;
            settings.Scale = scale;
            settings.Window = window;
            settings.TTFMode = TTFMode;
            settings.BkGMethod = BKGMethod;
            settings.Tforeground = Tforeground;
            settings.Tbackground = Tbackground;
            settings.TCNR = TCNR;
            settings.Override = Override;
            settings.fRange = fRange;
            settings.fSamples = fSamples;
            settings.Scale_CC = Scale_CC;
            settings.Verbose = Verbose;
            settings.Condition = Condition;
            varargout{i}=settings;
        case 3
            stats.f10 = getTTFCuttoff(f,TTF,.1);
            stats.f50 = getTTFCuttoff(f,TTF,.5);
            stats.CNR = CNRslice;
            stats.CNR_total = CNRall;
            stats.averageTargetHU = HUtarget;
            stats.averageBKGaroundTarget = bkgTarget;
            stats.noise=noise;
            stats.CNRtotalWarning=CNRtotalWarning;
            stats.CNRsliceWarning=CNRsliceWarning;
            stats.TTFModeChanged=TTFModeChanged;
            stats.ConditionChanged = ConditionChanged;
            stats.ConditionError = ConditionError;
            stats.nSlices = size(I,3);
            stats.ESF.r = r;
            stats.ESF.HU = HU;
            stats.ESF.distance = distance;
            stats.ESF.ESF = ESF;
            stats.center = mean(center,1) + [box(1) box(2)]-1;
            varargout{i} = stats;
    end
end



end

function [CNRall, CNRslice,noise,HU,bkg] = getCNR(I,mask,Tforeground,Tbackground)
%This function computes the CNR of each slice and also the global CNR of
%a slice-averaged image.

%Create a coordinate system
[X, Y]=meshgrid(1:size(I,2),1:size(I,1));
R=sqrt((X-size(I,2)/2).^2 +(Y-size(I,1)/2).^2);

%Compute the CNR of the slice-averaged image
im = mean(I,3);
bkg=mean(im(mask & R>quantile(R(mask),Tbackground)));
obj=mean(im(mask & R<quantile(R(mask),Tforeground)));
contrast=obj-bkg;
noise = std(im(mask & R>quantile(R(mask),Tbackground)));
CNRall = abs(contrast/noise);

%Compute the CNR of the individual slices
for i=1:size(I,3)
    im = I(:,:,i);
    bkg=mean(im(mask & R>quantile(R(mask),Tbackground)));
    obj=mean(im(mask & R<quantile(R(mask),Tforeground)));
    contrast=obj-bkg;
    noise = std(im(mask & R>quantile(R(mask),Tbackground)));
    CNR(i) = abs(contrast/noise);
    noise_all(i)=noise;
    HU_all(i) = obj;
    bkg_all(i) = bkg;
end
CNRslice = mean(CNR(i));
noise=mean(noise_all);
HU = mean(HU_all);
bkg = mean(bkg_all);
end

function im = subtractBackground(im,mask,method,Tbackground)
%this function removes background trends in the ROI using the specified
%method

%Create a coordinate system
[X, Y]=meshgrid(1:size(im,2),1:size(im,1));
X = repmat(X,[1 1 size(im,3)]);
Y = repmat(Y,[1 1 size(im,3)]);
R=sqrt((X-size(im,2)/2).^2 +(Y-size(im,1)/2).^2);

%get a background mask
bkg=mask & R>quantile(R(mask),Tbackground);

switch method
    case 'Poly' %Fit 2nd order 2D polynomial to background and subtrast
        x = X(bkg);
        y = Y(bkg);
        z = im(bkg);
        c=poly2fit(x,y,z,2);
        im = im-(c(1)*X.^2+c(2)*X.*Y+c(3)*Y.^2+c(4)*X+c(5)*Y+c(6));
        
    case 'Mean' %Subtract mean background value out
        im = im-mean(im(bkg));
        
    case 'None' %Does nothing to the image
        
end


end

function [CenterMode, scale, window, CenterMethod, TTFMode, BKGMethod, Tforeground, Tbackground, TCNR, Override, fRange, fSamples, Scale_CC, Verbose, Condition] = parseInputs(args)
%This function parses all the optional inputs

%Default settings
CenterMode = 'Slice';
CenterMethod = 'CrossCorrelation';
scale = 10;
window = 15;
TTFMode = 'Slice';
BKGMethod = 'Poly';
Tforeground = .2;
Tbackground = .8;
TCNR = 15;
Override = true;
fRange = [0 2];
fSamples = 256;
Scale_CC = 4;
Verbose = true;
Condition = false;

%loop through extra arguments and grab the values
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'CenterMode'
                CenterMode = args{i+1};
            case 'CenterMethod'
                CenterMethod = args{i+1};
            case 'Scale'
                scale = args{i+1};
            case 'Window'
                window = args{i+1};
            case 'TTFMode'
                TTFMode=args{i+1};
            case 'BKGMethod'
                BKGMethod = args{i+1};
            case 'Tforeground'
                Tforeground = args{i+1};
            case 'Tbackground'
                Tbackground = args{i+1};
            case 'TCNR'
                TCNR = args{i+1};
            case 'Override'
                Override = args{i+1};
            case 'fRange'
                fRange = args{i+1};
            case 'fSamples'
                fSamples = args{i+1};
            case 'Scale_CC'
                Scale_CC = args{i+1};
            case 'Verbose'
                Verbose = args{i+1};
            case 'Condition'
                Condition = args{i+1};
        end
    end
    
end

end


