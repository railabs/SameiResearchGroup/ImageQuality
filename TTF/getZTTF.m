% Copyright 20xx - 2019. Duke University
function [f, TTF, varargout] = getZTTF(I,ROI,psize,varargin)
%This function measures the z-direction Task Transfer Function (TTF) using
%an angled edge method
%--------------------------------------------------------------------------
%Syntax:
%   [f, TTF] = getZTTF(I,ROI,psize) computes the TTF for a stack of
%   images, I. ROI = [cx cy cz Rmin Rmax Rz] defines the origin (cx, cy, cz
%   in pixels), the min and max in-plane radius used (this limits the FOV
%   of the analysis area) and the distance from the edge (in mm) to use in
%   the z-direction. psize is the pixel size [px py pz].
%
%   [...] = getZTTF(...,name,value,...) uses optional settings to compute
%   the TTF.
%--------------------------------------------------------------------------
%Optional settings
%
%   Scale
%   This value controls the size of the bins for the re-binning and
%   averaging step of the ESF (default = 10). The bin size is equal to
%   psize/Scale (i.e., the bigger the scale, the more finely the ESF is
%   sampled).
%
%   Window
%   This value controls the width of the Hann window applied to the PSF
%   (default = 15).
%
%   fRange
%   1x2 vector describing the desired range of the output frequencies.
%   Default is [0 2];
%
%   fSamples
%   Integar value that controls the length/spacing of the output f and TTF.
%   Default is 128.
%
%   Verbose
%   Boolean that controls if warnings are given during execution of code.
%   Default is true.
%
%   Condition
%   Boolean that controls if the binned ESF is conditioned. Default is
%   false but if the CNRall is less than TCNR (and Override is true), then
%   Condition will be changed to true.
%
%   Sigma
%   Standard deviation of gaussian smoothing kernel used in classification
%   step (seperating foreground from background, part of fitting a plane to
%   the data) Defualt is 2.
%--------------------------------------------------------------------------
%Notes:
%   This code is a combination of codes written Jered Wells and Justin
%   Solomon. It is based on the paper cited below. This current
%   implementation was written by Justin Solomon
%
%   Chen, B., et al. (2014). Assessment of volumetric noise and resolution
%   performance for linear and nonlinear CT reconstruction methods.. Med
%   Phys, 41(7), 071909.
%
%   Version 1.0 - 3/29/15
%
%   Send questions to justin.solomon@duke.edu
%--------------------------------------------------------------------------

%get all the input parameters
[scale, window, fRange, fSamples, Verbose, Condition, Sigma] = parseInputs(varargin);

%ROI = [cx cy cz Rmin Rmax Rz]
cx = ROI(1);
cy = ROI(2);
cz = ROI(3);
Rmin = ROI(4);
Rmax = ROI(5);
Rz = ROI(6);

%Crop along z direction
z = ((1:size(I,3))-cz)*psize(3);
z  = find(abs(z)<=Rz);
I = I(:,:,min(z):max(z));
cz = cz - min(z)+1;

%Crop along x-y direction
x = ((1:size(I,2))-cx)*psize(1);
y = ((1:size(I,1))-cy)*psize(2);
[X,Y] = meshgrid(x,y);
[~,R] = cart2pol(X,Y);
mask = R>=Rmin & R<=Rmax;
[Y,X] = ind2sub(size(I),find(mask));
xmin = min(X(:)); xmax = max(X(:));
ymin = min(Y(:)); ymax = max(Y(:));
I = I(ymin:ymax,xmin:xmax,:);
mask = mask(ymin:ymax,xmin:xmax);
mask = repmat(mask,[1 1 size(I,3)]);
cy = cy-ymin+1;
cx = cx-xmin+1;

%make a coordinate system
x = ((1:size(I,2))-cx)*psize(1);
y = ((1:size(I,1))-cy)*psize(2);
z = ((1:size(I,3))-cz)*psize(3);
[X,Y,Z] = meshgrid(x,y,z);

%Classify voxels into the two materials using Otsu method
im = imgaussfilt3(I,Sigma); %Smooth the image
im = mat2gray(im,[min(im(mask)) max(im(mask))]); %Convert grayscale to 0-1
level = graythresh(im(mask)); %Get Otsu threshold
BW = im>level; %Apply Threshold
%Remove islands
CC = bwconncomp(BW);
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
BW=false(size(BW));
BW(CC.PixelIdxList{idx}) = true;
%Dilate and errode
BW = imerode(imdilate(BW,ones(5,5,5)),ones(5,5,5));

%Fit best plane
[class, ~, ~, ~, coeff] = classify([X(mask) Y(mask) Z(mask)],[X(mask) Y(mask) Z(mask)],BW(mask));
coeffs = [coeff(2,1).linear' coeff(2,1).const]; %Coefficients of plane in the form of 0 = ax + by + cz + d where coeffs = [a b c d];
plane = @(x,y) -((coeffs(1)*x+coeffs(2)*y+coeffs(4))./coeffs(3));

%get the contrast and noise
BW2=false(size(BW));
BW2(mask)=class;
BW3 = ~BW2 & mask;
contrast = abs(mean(I(BW2)) - mean(I(BW3)));
noise = std(I(BW3));
CNR = contrast/noise;

%get the raw ESF (pv vs distance)
if Verbose; disp('Getting ESF'); end
[r, HU] = getRawESF(I,X,Y,Z,mask,coeffs);

%bin and average the data
bsize = psize(3)/scale;
edges = -Rz:bsize:Rz;
[distance, ESF, variance] = rebinData(r,HU,edges);
%distance = distance-min(distance);

%condition if needed
if Condition
    try
        ESF = esfcond(ESF)';
        ConditionError = false;
    catch
        ConditionError = true;
    end
end

%convert the ESF to the TTF
if Verbose; disp('Getting TTF'); end
[f, TTF] = ESF2TTF(distance,ESF,bsize,window);

%Resample the TTF according to fRange and fSamples
f_int = linspace(fRange(1),fRange(2),fSamples);
TTF = interp1(f,TTF,f_int,'linear','extrap')';
f = f_int';

if Verbose; disp('Done!'); end

%Set optional outputs
n = nargout-2;
for i=1:n
    switch i
        case 1
            varargout{i}=contrast;
        case 2
            settings.Scale = scale;
            settings.Window = window;
            settings.fRange = fRange;
            settings.fSamples = fSamples;
            settings.Verbose = Verbose;
            settings.Condition = Condition;
            settings.Sigma = Sigma;
            varargout{i}=settings;
        case 3
            stats.f10 = getTTFCuttoff(f,TTF,.1);
            stats.f50 = getTTFCuttoff(f,TTF,.5);
            stats.CNR = CNR;
            stats.CNR_total = CNR;
            stats.noise=noise;
            stats.ConditionError = ConditionError;
            stats.nSlices = size(I,3);
            stats.ESF.r = r;
            stats.ESF.HU = HU;
            stats.ESF.distance = distance;
            stats.ESF.ESF = ESF;
            stats.PlaneCoeffs = coeffs;
            stats.Plane = plane;
            stats.center = [cx cy cz];
            varargout{i} = stats;
    end
end

end

function [scale, window, fRange, fSamples, Verbose, Condition, Sigma] = parseInputs(args)
%This function parses all the optional inputs

%Default settings
scale = 10;
window = 15;
fRange = [0 2];
fSamples = 128;
Verbose = false;
Condition = true;
Sigma = 2;

%loop through extra arguments and grab the values
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'Scale'
                scale = args{i+1};
            case 'Window'
                window = args{i+1};
            case 'fRange'
                fRange = args{i+1};
            case 'fSamples'
                fSamples = args{i+1};
            case 'Verbose'
                Verbose = args{i+1};
            case 'Condition'
                Condition = args{i+1};
            case 'Sigma'
                Sigma = args{i+1};
        end
    end
    
end

end

function [distance, ESF] = getRawESF(I,X,Y,Z,mask,coeffs)

%get the pixel values of interest
ESF = I(mask);

%get the positions of the pixels of interest
X = X(mask);
Y = Y(mask);
Z = Z(mask);

%compute the distance of pixels to plane
a = coeffs(1); b = coeffs(2); c = coeffs(3); d = coeffs(4);
num = (a*X + b*Y + c*Z + d);
den = sqrt(a^2+b^2+c^2);
distance  = num./den;
end

function pos = getScatteredPoints(im,p,maxPeaks,MinLength,X,Y,Z,psize)
im = permute(im,p);
X = permute(X,p);
Y = permute(Y,p);
Z = permute(Z,p);
pos = [];
for i=1:size(im,3)
    
    %find edge
    BW = edge(im(:,:,i));
    
    %compute hough transform
    [H,theta,rho] = hough(BW);
    
    %Get the peaks of the hough transform
    P = houghpeaks(H,maxPeaks);
    
    %get the line
    lines = houghlines(BW,theta,rho,P,'MinLength',MinLength);
    clear theta
    for j = 1:length(lines)
        theta(j) = lines(j).theta;
    end
    [~, ind] = min(abs(theta));
    line = lines(ind);      %this gets the most vertical line and gets rid of lines from the phantom edge
    
    %create the points along the line
    ind = [line.point1(2) line.point1(1) i];
    p1 = [X(ind(1),ind(2),ind(3)) Y(ind(1),ind(2),ind(3)) Z(ind(1),ind(2),ind(3))];
    ind = [line.point2(2) line.point2(1) i];
    p2 = [X(ind(1),ind(2),ind(3)) Y(ind(1),ind(2),ind(3)) Z(ind(1),ind(2),ind(3))];
    dist = norm(p1-p2);
    npoints = ceil(dist/mean(psize));
    xp = linspace(p1(1),p2(1),npoints);
    yp = linspace(p1(2),p2(2),npoints);
    zp = linspace(p1(3),p2(3),npoints);
    pos = [pos ; [xp' yp' zp'] ];
end
end



