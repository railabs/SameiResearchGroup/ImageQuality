% Copyright 20xx - 2019. Duke University
function [f, TTF] = ESF2TTF(R,ESF,bsize,window)
%this function converts an ESF to an LSF (derivative) and then to a TTF
%(Fourier Transform).

%Find the width of the ESF
M=max(ESF);
m=min(ESF);
[x,edgePosition] = sort(abs( ESF-(M+m)/2 ));% make sure ESF is increasing
edgeCenter = (edgePosition(1));
if mean(ESF(1:edgeCenter))>mean(ESF(edgeCenter:end))
    E1=find(ESF>m+0.85*(M-m),1,'last');
    E2=find(ESF<m+0.15*(M-m),1,'first');
else
    E1=find(ESF>m+0.85*(M-m),1,'first');
    E2=find(ESF<m+0.15*(M-m),1,'last');
end
w=window*abs(E2-E1);
f1=max(edgeCenter-w,1);
f2=min(edgeCenter+w,length(ESF)-1);

%Take derivative of ESF to get LSF
LSF = diff(ESF);
R_LSF= R(1:end-1);

%Apply hann window to smooth out tails
H = zeros(size(LSF));
H(f1:f2) = hann(length(H(f1:f2)));
LSF = LSF(:).*H(:);

%Compute TTF
TTF = abs(fft(LSF));
TTF = TTF(1:floor(length(TTF)/2));
TTF = TTF./TTF(1);
f = linspace(0,1/bsize/2,length(TTF))';

end