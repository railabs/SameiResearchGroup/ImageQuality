% Copyright 20xx - 2019. Duke University
function [center, contrast] = findCenter_CC(im,scale,rad,mask,Tforeground,Tbackground)
%This function finds the center (in pixels) of the rod using a cross
%correlation between the image and a circle of radius R (in pixels)

%Create a coordinate system
[X, Y]=meshgrid(1:size(im,2),1:size(im,1));
Cx = size(im,2)/2; Cy = size(im,1)/2;
X=X-Cx; Y=Y-Cy;
R = sqrt(X.^2+Y.^2);

%Compute the contrast of the rod
bkg=mean(im(mask & R>quantile(R(mask),Tbackground)));
obj=mean(im(mask & R<quantile(R(mask),Tforeground)));
contrast=obj-bkg;

%remove background and switch polarity of image if the contrast is negative
im = im-bkg;
im(~mask)=0;
if contrast<0
    im=im*-1;
end

%upsample the image by the scale factor so we can find the center on a
%subpixel level.
im = imresize(im,scale,'method','bilinear');
X = imresize(X,scale,'method','bilinear');
Y = imresize(Y,scale,'method','bilinear');
R = sqrt(X.^2+Y.^2);

%Make image of the circle
circle = double(R<=rad);
circle=circle/sum(circle(:));

%Run filter in Fourier Domain
MV = mean(im(:));
im = im-MV;
F=fftn(im);
F(1)=0;
H = fftn(circle);
H=F.*H;
cc = fftshift(real(ifftn(H)) + MV );
[~, imax] = max(cc(:));
[ypeak, xpeak] = ind2sub(size(cc),imax(1));
center = ([xpeak ypeak]./scale)+1;

end