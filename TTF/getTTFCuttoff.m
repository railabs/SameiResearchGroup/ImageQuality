% Copyright 20xx - 2019. Duke University
function fcut = getTTFCuttoff(f,TTF,threshold)
%This function computes the frequency at which the TTF achieves the value
%defined by the threshold (e.g., if threshold = .5, then fcut is the 50%
%frequency of the TTF)

%get the index of the TTF closest to the threshold 
ind = find(TTF<threshold,1);
if ~isempty(ind)
    %get the surrounding points
    if ind==1
        fcut = nan;
    else
        imin= ind-1;
        imax = ind;
        %Linear interpolate to find f where TTF=threshold
        xmin=f(imin);
        xmax=f(imax);
        ymin=TTF(imin);
        ymax=TTF(imax);
        m = (ymax-ymin)/(xmax-xmin);
        b = ymin - m*xmin;
        fcut = (threshold - b)/m; 
    end
else
    fcut = nan;
end



end