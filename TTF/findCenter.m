% Copyright 20xx - 2019. Duke University
function [center, contrast] = findCenter(im,mask,Tforeground,Tbackground)
%This function finds the center of the rod in pixel units

%Create a coordinate system
[X, Y]=meshgrid(1:size(im,2),1:size(im,1));
R=sqrt((X-size(im,2)/2).^2 +(Y-size(im,1)/2).^2);

% Segment the rod out using Otsu threshold
amin=min(im(mask));
amax=max(im(mask));
sim=mat2gray(im,[amin amax]);
t=graythresh(sim(mask)); %Otsu threshold
bw=im2bw(sim,t);
bw=bw & mask;
bw=bwareaopen(bw,100); % remove connected contents with less than 100 pixels. a larger number here will help remove the bed

%Compute the contrast of the rod
bkg=mean(im(mask & R>quantile(R(mask),Tbackground)));
obj=mean(im(mask & R<quantile(R(mask),Tforeground)));
contrast=obj-bkg;
if contrast<0
    bw=~bw & mask;
end

%mask the background data
im(bw) = 0;

%compute the centroid of the rod
center=zeros(1,2);
center(1)= (sum(sum(X.*im))/sum(sum(im))); % X coordinate
center(2)= (sum(sum(Y.*im))/sum(sum(im))); % Y coordinate

end