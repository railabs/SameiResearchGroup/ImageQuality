% Copyright 20xx - 2019. Duke University
function varargout = esfcond(esf,varargin)
% ESFCOND conditions ESF according to monotonicity requirement of Maidment
%   ESFCOND(ESF) conditions the input edge spread function ESF according to
%   work in Maidment and Albert (2003). This requires that the noise-free 
%   ESF is monotonically increasing. Given this condition, ESFCOND finds 
%   the solution with minimum Chi-square parameter: X2 = SUM((yi-Yi).^2).
%
%   ESFCOND(ESF,W) allows the user to define arbitrary weights for the
%   elements in ESF. Maidment and Albert suggest that better results may be
%   obtained by setting W to the inverse of the variance at every point.
%   By default, W is set to unity at every point.
%
%   EXAMPLE
%   a = zeros(276,1);
%   a(140:end) = 1;
%   b = conv(a,fspecial('gaussian',[19,1],2),'same');
%   b = b(11:266);
%   bn = b + randn(256,1)./50;
%   c = esfcond(bn);
%   figure; plot(b); hold on; plot(bn,'r-'); plot(c,'g-')
%   legend('Original','Noisy','Conditioned','Location','Best')
%   title('ESF')
%

%   Jered R Wells
%   04/02/12
%   jered [dot] wells [at] duke [dot] edu
%
%   v 1.1 (01/10/13)
%
%   Ref: A. D. Maidment and M. Albert, "Conditioning data for calculation
%   of the modulation transfer function," Med Phys 30, 248-253 (2003).

% CHECK INPUTS
narginchk(1,2);
nargoutchk(0,1);
fname = 'esfcond';
inputcheck(esf,{'numeric'},{'real','nonnan','nonempty','vector'},fname,'ESF',1);

% Set defaults for optional inputs
optargs = {ones(size(esf))};
numvarargs = length(varargin);
for ii = 1:numvarargs; if isempty(varargin{ii}); varargin{ii} = optargs{ii}; end; end

% Now put these defaults into the optargs cell array, 
% and overwrite the ones specified in varargin.
optargs(1:numvarargs) = varargin;
% or ...
% [optargs{1:numvarargs}] = varargin{:};

% Place optional args in memorable variable names
[w] = optargs{:};

inputcheck(w,{'numeric'},{'real','nonnan','nonempty','vector','numel',numel(esf)},fname,'W',2);

% INITIALIZE

% Check the ESF and make sure that it is rising from left to right
ctr = ceil((length(esf) + 1)/2);
if mean(esf(1:ctr))>=mean(esf(ctr:end)); 
    esf = flipud(esf(:)); 
    w = flipud(w(:));
end

% MAIN BODY
Z = esf(:);
w = w(:);
ind = (1:length(Z))';
test = Z(1:end-1)>Z(2:end);

% Begin the iterative ESF (Z) conditioning loop
% Terminate when the conditioned ESF (Z) is monotonically increasing
while any(test)
    ii = 1;
    while ii<length(Z)-1
        if Z(ii)>Z(ii+1)
            Z(ii) = (w(ii).*Z(ii) + w(ii+1).*Z(ii+1))./(w(ii) + w(ii+1));
            w(ii) = w(ii) + w(ii+1);
            Z = [Z(1:ii);Z(ii+2:end)];
            w = [w(1:ii);w(ii+2:end)];
            ind = [ind(1:ii); ind(ii+2:end)];
        else
            ii = ii + 1;
        end
    end
    ii = numel(Z)-1;
    if Z(ii)>Z(ii+1)
        Z(ii) = (w(ii).*Z(ii) + w(ii+1).*Z(ii+1))./(w(ii) + w(ii+1));
        w(ii) = w(ii) + w(ii+1);
        Z = Z(1:ii);
        w = w(1:ii);
        ind = ind(1:ii);
    end
    % Update the TEST variable
    test = Z(1:end-1)>Z(2:end);
end

% Generate final result
res = zeros(size(esf));
for ii = 1:length(ind)-1
    res(ind(ii):ind(ii+1)-1) = Z(ii);
end
res(ind(end):end) = Z(end);

varargout = {res};

end % ESFCOND