% Copyright 20xx - 2019. Duke University
function [d, AUC, AUC_CI] = getCHOPerformance(gs, gb,alpha)
%gs and gb are the nchannels x nrealization matrices of channel outputs
%under each condition
%S is the intraclass scatter matrix (nchannels x nchannels)
%alpha is the weighting factor for the internal noise.

m = size(gs,2);
n = size(gb,2);
p = size(gs,1);
bias = (m+n)*p/(m*n);

%add internal noise
if alpha >0
    nRealizations = 100;
    parfor i=1:nRealizations
        %get variance
        vars = alpha*var(gs,[],2);
        varb = alpha*var(gb,[],2);
        
        %generate random nose
        noises = random('norm',zeros(size(gs)),sqrt(repmat(vars,[1 size(gs,2)])));
        noiseb = random('norm',zeros(size(gb)),sqrt(repmat(vars,[1 size(gb,2)])));
        
        %add noise to channelized data
        gstemp =gs + noises;
        gbtemp = gb + noiseb;
        
        %compute the performance for this noise realization
        [ret] = exactCI_CHO(.025,.025,gstemp,gbtemp);
        d(i,1) = mean(ret.SNR_CI);
        AUC(i,1) = mean(ret.AUC_CI);
        AUC_CI(i,:) = ret.AUC_CI;
    end
    d=mean(d);
    AUC=mean(AUC);
    AUC_CI = mean(AUC_CI,1);
else
    [ret] = exactCI_CHO(.025,.025,gs,gb);
    d = mean(ret.SNR_CI);
    AUC = mean(ret.AUC_CI);
    AUC_CI = ret.AUC_CI;
end

end