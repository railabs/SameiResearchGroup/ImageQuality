% Copyright 20xx - 2019. Duke University
function [d, MTF, fr, df, W, CI, AUC, AUC_CI] = getDprime2d(fMTF,MTF,fNPS,NPS,task,FOV,display,distance,ObserverType,variance,alpha)

%Create 2D MTF
[U V]=meshgrid(fNPS,fNPS);
fr=sqrt(U.^2+V.^2);
MTF=interp1(fMTF,MTF,fr,'linear',0);

%Create task function task = [contrast radius]
W = task(1)*(sqrt(3*task(2))./(4.*fr)).*besselj(1,2*pi*task(2).*fr);

%Get visual response function
switch ObserverType
    case 'NPW'
        E=ones([size(NPS,1) size(NPS,2)]);
        N = zeros(size(fr));
    case 'NPWE'
        E=getVisualResponseFunction(fr,FOV,display,distance);
        N = zeros(size(fr));
    case 'NPWEi'
        E=getVisualResponseFunction(fr,FOV,display,distance);
        N=alpha.*((distance./1000).^2).*variance.*ones(size(fr)); %based on Richard, S., & Siewerdsen, J. H. (2008). Comparison of model and human observer performance for detection and discrimination tasks using dual-energy x-ray images. Medical Physics, 35(11), 5043. doi:10.1118/1.2988161
    case 'NPWi'
        E=ones([size(NPS,1) size(NPS,2)]);
        N=alpha.*((distance./1000).^2).*variance.*ones(size(fr));
    otherwise
        warning([ObserverType ' is not a valid ''ObserverType''. Using ''NPW'' observer'])
        E=ones([size(NPS,1) size(NPS,2)]);
end


%Calculate d'
df=abs(fNPS(2)-fNPS(1));
for i=1:size(NPS,3)
    nps =NPS(:,:,i);
    num=(sum(sum(   (W.^2).*(MTF.^2).*(E.^2)  )).*df*df).^2;
    den=(sum(sum(   (W.^2).*(MTF.^2).*(E.^4).*(nps) + N )).*df*df);
    D(i) = sqrt(num./den);
end
d=mean(D);
temp=1.96 * (std(D)./sqrt(size(NPS,3)));
CI=[d-temp d+temp];
AUC = normcdf(d/sqrt(2));
AUC_CI=normcdf(CI/sqrt(2));


end