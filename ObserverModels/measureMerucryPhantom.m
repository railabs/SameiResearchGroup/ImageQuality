% Copyright 20xx - 2019. Duke University
function [ results ] = measureMerucryPhantom(input,output)
%Measures NPS, MTF, and d' using CT images of the mercury phantom
%   input is the directory containing the DICOMS
%   output is the directory where output files will be saved

xSize=10;               %size (inches) of the figures created
ySize=7;                %size (inches) of the figures created
fSize=10;               %font size for figures
dmax=64;                %maximum correlation length for NPS
pad=128;                %Size of 2D NPS after FFT
bin=.25;                %Size of subpixels (bin should be less than 1)
display=305;            %Size of the displayed image (in mm)
distance=650;           %Distance of observer to display (in mm)
tasks=(1:.5:10)';


[pathstr,name,ext] = fileparts(input);

%Read the DICOM series
disp([name ': Reading DICOMS'])
[I info slices]=readCTSeries(input);
psize=info(1).PixelSpacing(1);

%Get the metadata from header
disp([name ': Getting metadata'])
results.name=name;
results.scanner=info(1).ManufacturerModelName;
results.kVp=info(1).KVP;
results.filter=info(1).FilterType;
results.recon=info(1).ConvolutionKernel;
results.SliceThickness=info(1).SliceThickness;
results.psize=psize;
FOV=info(1).ReconstructionDiameter;
for i=1:length(info)    %Get CTDI info
    try
        results.CTDI(i)=info(i).CTDIvol;
    catch
        if i==1
            warning('CTDI not in Dicom Header, saving effective mAs')
        end
        try
            results.CTDI(i)=info(i).XrayTubeCurrent*(info(i).ExposureTime/1000)/info(i).SpiralPitchFactor;
        catch
            results.CTDI(i)=info(i).XrayTubeCurrent*(info(i).ExposureTime/1000);
        end
    end
end
results.CTDI_mean=mean(results.CTDI);
if std(results.CTDI)>.01;
    results.AEC='Yes';
else
    results.AEC='No';
end

%Load the ROI info
dir_ROI=dir([input '/*ROI*.mat']);
foundROI=false;
i=1;
while ~foundROI
    try
        load([input '/' dir_ROI(i).name])
        foundROI=true;
    catch
        i=i+1;
        if i>length(dir_ROI)
            foundROI=true;
            warning('Could not find ROI')
        end
    end
end


%Create coordinate system
x=1:size(I,2); y=1:size(I,1);
[X Y]=meshgrid(x,y);

%Calculate NPS for each ROI
for i=1:size(ROI.NPS,1)
    disp([name ': Getting NPS, Size ' num2str(i) '/' num2str(size(ROI.NPS,1))])
    cx=ROI.NPS(i,1);    cy=ROI.NPS(i,2);
    rmin=ROI.NPS(i,3);  rmax=ROI.NPS(i,4);
    zmin=ROI.NPS(i,5);  zmax=ROI.NPS(i,6);
    R=sqrt((X-cx).^2 +(Y-cy).^2);
    mask= R>=rmin & R<=rmax;
    [NPS(:,:,i) f variance(i)] = getMercuryNPS2D(I,mask,dmax,psize,pad,zmin,zmax);
end
%Get radial averaged NPS
[U V]=meshgrid(f,f);
fr=sqrt(U.^2+V.^2); fr=fr(:);
for i=1:size(NPS,3)
    nps=NPS(:,:,i); nps=nps(:);
    [fNPS_r NPS_r(i,:)] = Binning_v2(pad, fr,nps);
end
fNPS_2D=f;

%Create NPS figure
fig = makeNPSfigure(I,fNPS_2D,NPS,fNPS_r,NPS_r,variance,ROI,xSize,ySize,fSize,name,output);
close(fig)
drawnow

%Calculate the MTF for each insert and for each size
disp([name ': Getting TTFs'])
for i=1:size(ROI.MTF,1)
    for j=1:size(ROI.MTF,3)
        cx=ROI.MTF(i,1,j);      cy=ROI.MTF(i,2,j);
        rmin=ROI.MTF(i,3,j);    rmax=ROI.MTF(i,4,j);
        zmin=ROI.MTF(i,5,j);    zmax=ROI.MTF(i,6,j);
        %[MTF(i,:,j) f(i,:,j) cond{i,j} contrast(i,j)] = getMercuryMTF2D(I,cx,cy,rmin,rmax,zmin,zmax,psize,bin,pad);
        TTF_ROI = [cx cy rmax*psize 12.5];
        [ftemp, TTF, contrast(i,j), settings(i,j), stats(i,j)] = get2DTTF(I(:,:,zmin:zmax),TTF_ROI,psize,'Verbose',false,'TTFMode','Mean','Override',true,'Condition',true,'Tbackground',.6,'Tforeground',.1);
        MTF(i,:,j) = TTF';
        f(i,:,j) = ftemp';
        
    end
end
results.contrast=contrast;

%Create MTF figure
fig = makeMTFfigure(I,f,MTF,ROI,xSize,ySize,fSize,name,output,contrast);
close(fig)
drawnow

%Calculate d' for each insert and size
disp([name ': Getting d-primes'])
for i=1:size(ROI.MTF,1)         %i is index over phantom sizes
    for j=1:size(ROI.MTF,3)     %j is index over inserts
        for k=1:size(tasks,1)   %k is index over task functions
            [results.d_prime.NPWE(i,j,k), ~, ~, ~, ~] = getDprime2d(f(i,:,j),MTF(i,:,j),fNPS_2D,NPS(:,:,i),[contrast(i,j) tasks(k)],FOV,display,distance,'NPWE');
            [results.d_prime.NPW(i,j,k), ~, ~, ~, ~] = getDprime2d(f(i,:,j),MTF(i,:,j),fNPS_2D,NPS(:,:,i),[contrast(i,j) tasks(k)],FOV,display,distance,'NPW');
            if ~isreal(results.d_prime.NPWE(i,j,k)) || ~isreal(results.d_prime.NPW(i,j,k))
                disp([i j k])
            end
        end
    end
end

E=getVisualResponseFunction(fNPS_r,FOV,display,distance);

fig = makedprimefigure(results.d_prime.NPWE,ROI,tasks,xSize,ySize,fSize,name,output,3,1:2:length(tasks),'NPWE');
close(fig);
drawnow

%Save outputs
results.ROI=ROI;
results.dmax=dmax;
results.pad=pad;
results.bin=bin;
%results.Conditioned=cond;
results.tasks=tasks;
results.NPS.NPS=NPS;
results.NPS.f=fNPS_2D;
results.NPS.NPS_r=NPS_r;
results.NPS.f_r=fNPS_r;
results.NPS.variance=variance;
results.MTF.MTF=MTF;
results.MTF.f=f;
results.MTF.settings=settings;
results.MTF.stats=stats;
results.HVR.HVR=E;
results.HVR.f=fNPS_r;
results.HVR.distance=distance;
results.HVR.display=display;
results.HVR.FOV=FOV;


save([output name '_results.mat'],'results')

end

function [I info slices]=readCTSeries(input)
dir_old=pwd;
cd(input);
list=dir('*.IMA');
if length(list)==0
    list=dir('*.dcm');
end
if length(list)==0
    list=dir('IM*');
end
I=zeros(512,512,length(list));
slice=zeros(length(list),1);
for i=1:length(list)
    try
        info(i)=dicominfo(list(i).name);
    catch
        temp=dicominfo(list(i).name);
        info(i)=info(i-1);
        info(i).SliceLocation=temp.SliceLocation;
        info(i).XrayTubeCurrent=temp.XrayTubeCurrent;
        
    end
    
    I(:,:,i)=dicomread(info(i));
    slice(i)=info(i).SliceLocation;
end

[slices,IX] = sort(slice);
info=info(IX);
I=I(:,:,IX);
I=I*info(1).RescaleSlope+info(1).RescaleIntercept;
cd(dir_old);
end

function [NPS, f, variance] = getMercuryNPS2D(I,mask,dmax,psize,pad,zmin,zmax)

%Crop in the z direction
I=I(:,:,zmin:zmax);

%Crop with bounding box containing the mask (saves compuation time)
[Y, X]=ind2sub(size(mask),find(mask));
I=I(min(Y):max(Y),min(X):max(X),:);
mask=mask(min(Y):max(Y),min(X):max(X),:);

%Take autocorrelation of mask (gives the number of pixels used for each lag
%position of the autocorrelation function)
nPix=xcorr2(double(mask),double(mask));
zero_mask=nPix==0;

%create coordinate system
x=1:size(I,2); y=1:size(I,1);
[X Y]=meshgrid(x,y);
X=X(mask); Y=Y(mask);

%loop through, subract polynomial fit and calculate autocorrelation

for i=1:size(I,3)-2
    im=(I(:,:,i)-I(:,:,i+2))./sqrt(2);
    pval=im(mask);
    c=poly2fit(X,Y,pval,2);
    im(mask)=pval-(c(1)*X.^2+c(2)*X.*Y+c(3)*Y.^2+c(4)*X+c(5)*Y+c(6)); 
    im(~mask)=0;
    C(:,:,i)=xcorr2(im,im);
end
C=mean(C,3)./nPix;
C(zero_mask)=0;
C(isnan(C))=0;
C=C(size(I,1)-dmax:size(I,1)+dmax,size(I,2)-dmax:size(I,2)+dmax);
NPS = (psize.^2).*fftshift(abs(fft2(C,pad,pad)));
ny=1/(2*psize);
f=linspace(-ny,ny,size(NPS,1));
df=f(2)-f(1);
variance=sum(NPS(:)*df*df);

end

function p=poly2fit(x,y,z,n)
% POLY2FIT	POLY2FIT(x,y,z,n) finds the coefficients of a two-dimensional
%		polynomial formed from the data in the matrices x and y
%		that fits the data in the matrix z in a least-squares sense.
%		The coefficients are returned in the vector p, in descending
%		powers of x and y.  For example, the second order polynomial
%
%		x^2+xy+2x-3y-1
%
%		would be returned as [1 1 0 2 -3 -1]
%
%	Written by Jim Rees, Aug.12, 1991
%	Adapted by Jeff Siewerdsen

if any((size(x)~=size(y)) | size(x)~=size(z))
     error('X,Y, and Z matrices must be the same size')
end;

% First straighten out x,y, and z.
x=x(:);y=y(:);z=z(:);

n=n+1;
k=1;
A=zeros(size(x));
for i=n:-1:1,
     for j=1:i,
          A(:,k)=((x.^(i-j)).*(y.^(j-1)));
          k=k+1;
     end
end
p=(A\z).';
end

function [ c ] = designerNodule(r,R,n,C,HUmin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

c=(1-(r./R).^2).^n;
c(r>R)=0;
c=c*C+HUmin;

end

function [xBinned, yBinned] = Binning_v2(binNum, x, y)

binStart = min(x);
binEnd = max(x);
binEdge = linspace(binStart,binEnd,binNum+1);

xBinned = binEdge(1:end-1)+(binEdge(2)-binEdge(1));
yBinned = zeros(1,binNum);

[binHeight,whichBin] = histc(x,binEdge);
for i=1:binNum
    binMembers = y(whichBin == i);
    yBinned(i) = mean(binMembers); % this might create NaN 
end

% NAN will appear after "mean" if binMember is empty, use values of neighboring bin instead
yBinned = interp1(find(~isnan(yBinned)),yBinned(~isnan(yBinned)),1:binNum,'nearest','extrap');
end

function d_prime = d_calc_v2(W_radial,fW_radial,MTF_radial,fMTF_radial,NPS_radial,fNPS_radial,N0,u,ObserverType)

% interpolate
W_dprime = interp1(fW_radial,W_radial,u,'linear',0); 
MTF_dprime = interp1(fMTF_radial,MTF_radial,u,'linear',0); 
NPS_dprime = interp1(fNPS_radial,NPS_radial,u,'linear',0); 
du = u(2)-u(1);

% Eye filter 
% E(f) = f^1.3 * exp(-c*f^2)
% formula from Burgess's paper, mentioned in Ehsan's paper "Detection of subtle lung nodule"
% Assuming 100cm viewing distance and 40 cm 
fp = 4; % peak frequency in cycle/degree
Distance = 1000; % viewing distance in mm
c = 1.3/(2*fp^2)*(Distance*pi/180)^2; 
E = u.^1.3.*exp(-c*u.^2);
% figure;plot(u,E);

% Internal noise
% ref: A. E. Burgess, �Visual Perception Studies and Observer Models in Medical Imaging,?Seminars in Nuclear Medicine, 41(6), 419-436 (2011).
% Ni1 = (1+0.6)NPS+sigma1
Ni1 = 0.6*NPS_dprime;
% ref: A. E. Burgess, X. Li, and C. K. Abbey, �Visual signal detectability with two noise components: anomalous masking effects,?J Opt Soc Am A Opt Image Sci Vis, 14(9), 2420-42 (1997).
% Ni2 = 0.02N0D^2. N0 is the image white-noise spectral density and D is the viewing distance in meters. the D is the viewing distance in meter. 
Ni2 = 0.001*N0*(Distance/1000)^2;


% d' calculation
if strcmp(ObserverType,'Ideal')
    d_prime = sqrt( 2*pi*sum( (W_dprime.^2).*(MTF_dprime.^2)...
        ./NPS_dprime...
        .*u *du  )); % polar summation
elseif strcmp(ObserverType,'NPW')
    d_prime = sqrt( (2*pi*sum((W_dprime.^2).*(MTF_dprime.^2).*u.*du)).^2 ./ ...
        (2*pi*sum(NPS_dprime.*(W_dprime.^2).*(MTF_dprime.^2).*u.*du )) );
elseif strcmp(ObserverType,'NPWi')
    d_prime = sqrt( (2*pi*sum((W_dprime.^2).*(MTF_dprime.^2).*u.*du)).^2 ./ ...
        (2*pi*sum((NPS_dprime.*(W_dprime.^2).*(MTF_dprime.^2)+Ni1).*u.*du )) );
elseif strcmp(ObserverType,'NPWEi')
    d_prime = sqrt( (2*pi*sum((E.^2).*(W_dprime.^2).*(MTF_dprime.^2).*u.*du)).^2 ./ ...
        (2*pi*sum((NPS_dprime.*(E.^4).*(W_dprime.^2).*(MTF_dprime.^2)+Ni1).*u.*du)));
end
end

function [MTF, f, cond, C] = getMercuryMTF2D(I,cx,cy,rmin,rmax,zmin,zmax,psize,bin,pad)

[X Y]=meshgrid(1:size(I,2),1:size(I,1));
R=sqrt((X-cx).^2 +(Y-cy).^2);
mask=R<=rmax;
n=0;

%loop through each slice, find center, and get distance vs center
HU=[]; r=[];

%run these two lines if you want to averages slices before making ESF
I=mean(I(:,:,zmin:zmax),3);
zmin=1; zmax=1;

for i=zmin:zmax
    n=n+1;
    im=I(:,:,i);
    
    %find center
    [center C(n)] = center_localizer_v2(im,mask,R);
    Cx=center(1); Cy=center(2);
    
    %Get pixels of interest and add them to the list
    R2=sqrt((X-Cx).^2 +(Y-Cy).^2);
    mask2=R2>=rmin & R2<=rmax;
    HU=[HU; im(mask2)];
    r=[r; R2(mask2)];
end
C=mean(C);

[r esf] = Binning_v2((1/bin)*rmax, r, HU);

%condition ESF
try
    [esf,r] = esfcond_v6(esf,r,0);
    esf=esf'; r=r';
    cond='Yes';
catch
     cond='No';
end

[MTF,f]=ESF2MTF(esf,r*psize,pad);

% for i=1:length(esf)-1
%     lsf(i)=(esf(i+1)-esf(i))/(r(i+1)-r(i));
% end
% lsfIndex = 1:length(lsf);
% l=int16(length(lsf));
% h=hann(length(lsf)); % hann??? why in spatial domain
% h=(h.^2)';
% f = round(sum(lsf.*lsfIndex)/sum(lsf)); % use weighted center to make sure the peaks of lsf and filter line up. 
% f = mean(find(abs(lsf)==max(abs(lsf)))); % use max 
% if isinteger(l/2)
%     shift=l/2-f;
% else
%     shift=(l+1)/2-f;
% end
% if shift>=0
%     lsf=lsf([end-shift+1:end 1:end-shift]);
% else
%     shift=abs(shift);
%     lsf=[lsf(shift+1:end) fliplr(lsf(1:shift))];
% end
% lsf=lsf.*h; % get rid of the noise
% 
% % calculate mtf
% mtf_radial=abs(fftshift(fft(double(lsf),2*pad)));
% mtf_radial=mtf_radial(floor(length(mtf_radial)/2)+1:end);
% mtf_radial=mtf_radial/mtf_radial(1);
% 
% % determine the sp corresponding to MTF
% psize_smoothed = (r(2)-r(1))*psize;
% f = linspace(0,1/psize_smoothed/2,pad);      
% 

end

function [MTF,f]=ESF2MTF(ESF,R,pad)

[R,I]=sort(R);
ESF=ESF(I);

M=max(ESF);
m=min(ESF);
[x,edgePosition] = sort(abs( ESF-(M+m)/2 ));% make sure ESF is increasing
edgeCenter = (edgePosition(1));
if mean(ESF(1:edgeCenter))>mean(ESF(edgeCenter:end))
    E1=find(ESF>m+0.85*(M-m),1,'last');
    E2=find(ESF<m+0.15*(M-m),1,'first');
else
    E1=find(ESF>m+0.85*(M-m),1,'first');
    E2=find(ESF<m+0.15*(M-m),1,'last');
end

w=3*abs(E2-E1);
f1=max(edgeCenter-w,1);
f2=min(edgeCenter+w,length(ESF)-1);



%Calculate the LSF
LSF = diff(ESF); R_LSF = R(1:end-1);


H = zeros(size(LSF));
H(f1:f2) = hann(length(H(f1:f2)));

LSF = LSF(:).*H(:);

% MTF = abs(fftshift(fft(LSF,2*pad)));
% f = linspace(-1/(R_LSF(2)-R_LSF(1))/2,1/(R_LSF(2)-R_LSF(1))/2,length(MTF));
% i0=find(f>=0,1,'first');
% MTF=MTF/MTF(i0);
% MTF=MTF(i0:end);
% f=f(i0:end);

MTF = abs((fft(LSF,2*pad)));
MTF = MTF(1:floor(length(MTF)/2));
MTF = MTF./MTF(1);
f = linspace(0,1/(R_LSF(2)-R_LSF(1))/2,length(MTF));

end

function [center C] = center_localizer_v2(im,mask,R)
% Find the exact center of rod crosssection for MTF measurement
% sign determine which part of the binary image is used for centroid calculation

amin=min(im(mask));
amax=max(im(mask));
sim=mat2gray(im,[amin amax]);
t=graythresh(sim(mask));

bw=im2bw(sim,t); % create binary image
bw=bw & mask;
bw=bwareaopen(bw,100); % remove connected contents with less than 1000 pixels. a larger number here will help remove the bed

bkg=mean(im(mask & R>quantile(R(mask),.8)));
obj=mean(im(mask & R<quantile(R(mask),.2)));
C=obj-bkg;

if C<0
    bw=~bw & mask;
end

im = im .* bw; 

center=zeros(1,2);
[X,Y]=meshgrid(1:size(im,1));
center(1)= (sum(sum(X.*im))/sum(sum(im))); % X coordinate
center(2)= (sum(sum(Y.*im))/sum(sum(im))); % Y coordinate
end

function [res,d_res] = esfcond_v6(esf,d_esf,EdgeEnhancementStrength,w)
% ESFCOND conditions ESF according to monotonicity requirement of Maidment
%   ESFCOND(ESF) conditions the input edge spread function ESF according to
%   work in Maidment and Albert (2003). This requires that the noise-free
%   ESF is monotonically increasing. Given this condition, ESFCOND find the
%   solution with minimum Chi-square parameter: X2 = SUM((yi-Yi).^2).
%
%   ESFCOND(ESF,W) allows the user to define arbitrary weights for the
%   elements in ESF. Maidment and Albert suggest that better results may be
%   obtained by setting W to the inverse of the variance at every point.
%   By default, W is set to unity at every point.
%
%   Jered R Wells
%   04/02/12
%   jered [dot] wells [at] duke [dot] edu
%
%   v 1.0
%   Ref: A. D. Maidment and M. Albert, "Conditioning data for calculation
%   of the modulation transfer function," Med Phys 30, 248-253 (2003).
%
% v 2.0
% Modified by Olav Christianson
% Use 'pchip' interpolation to give a smoother output
% added the ability to accept non-monotonic inputs and inputs that are not
% evenly sampled
% 04/03/2012
%
% v 3.0
% Modified by Baiyu Chen
% Assuming asymetrical ESF when determining the mono tread
% Make sure esf and r are all colomn vectors
% Fix the typo when fliping the ESF
% eliminate starting and ending points of smoothed ESF to eliminate artifact
% 04/08/2012 and 04/22/2012
%
% v 4.0
% Modified by Baiyu Chen
% now can find the approximate position of the edge automatically. do not
% rely on the input of diskSize
%
% v 5.0
% Modified by Baiyu Chen
% Now the mono trend is not required near the edge to preserve the edge
% enhancement in IR
%
% v 6.0
% Modified by Baiyu Chen
% Now the mono trend is not required around the edge enhancement area only


% CHECK INPUTS
% Checks for multiple esf values at same distance
d_esf2 = unique(d_esf);
esf2 = zeros(size(d_esf2));
for i = 1:length(esf2)
    esf2(i) = mean(esf(d_esf2(i)==d_esf));
end
esf = esf2;
d_esf = d_esf2;

% check if weithing is provided
if nargin==3
    w = ones(size(esf));
elseif nargin==4
    if isempty(w); w = ones(size(esf));
    end
    if numel(w)~=numel(esf);
        error 'W must contain the same number of elements as ESF';
    end
else error 'Invalid number of input arguments specified'
end

% make sure it is a column vector
esf = esf(:);
d_esf = d_esf(:);
w = w(:);

% Sort data by distance
Z = esf;
d = d_esf;
[d,I] = sort(d);
Z = Z(I);
w = w(I);

flip = 0;

[x,edgePosition] = sort(abs( Z-(max(Z)+min(Z))/2 ));% make sure ESF is increasing
edgeCenter = d(edgePosition(1));
if mean(Z(d<edgeCenter)) >= mean(Z(d>edgeCenter))
    Z = flipud(Z);
    w = flipud(w);
    
    dd = diff(d);    % this part is necessary for unevenly spaced ESF
    dd = flipud(dd);
    d = ones(size(d))*min(d);
    cs = cumsum(dd);
    d(2:end) = d(2:end)+cs;
    
    esf = flipud(esf);
    dd = diff(d_esf);
    d_esf = ones(size(d_esf))*min(d_esf);
    dd = flipud(dd);
    cs = cumsum(dd);
    d_esf(2:end) = d_esf(2:end)+cs;
    
    flip = 1;
end

% start smoothing
% locate approximate location of the edge
edgeMag = max(Z)-min(Z);
edgeThreshold = EdgeEnhancementStrength*edgeMag;

edgeCenter = d(find( Z>(max(Z)+min(Z))/2, 1, 'first'));
edgeLeft = d(find( Z<=(min(Z)+edgeMag*0.05),1,'last'));
edgeRight = d(find( Z>=(max(Z)-edgeMag*0.05),1,'first'));
edgeWidth = edgeRight-edgeLeft;

cutoff1 = edgeCenter-3.5*edgeWidth; % in the unit of mm, not pixel
cutoff2 = edgeCenter+3.5*edgeWidth;
cutoff3 = edgeCenter-7*edgeWidth;
cutoff4 = edgeCenter+7*edgeWidth;
cutoff5 = edgeLeft;
cutoff6 = edgeRight;

f1 = find((d<=cutoff1),1,'last'); % in unit of pixel
f2 = find((d>=cutoff2),1,'first')-1;
f5 = find((d<=cutoff5),1,'last');
f6 = find((d>=cutoff6),1,'first')-1;

if isempty(f1)
    f1 = 1;
end
if isempty(f2)
    f2 = length(d)-1;
end
if f5==length(Z)
    f5=length(Z)-1;
end
if isempty(f6)
    f6 = 1;
end

% set mono rules
test = Z(1:end-1)-Z(2:end)>0; % check whether the over trend is mono
test_edge_left = Z(f1:f5)-Z(f1+1:f5+1)>edgeThreshold; % check whether the edge is semi-mono within the cutoff
test_edge_right = Z(f6:f2)-Z(f6+1:f2+1)>edgeThreshold; % check whether the edge is semi-mono within the cutoff
test(f1:f5) = test(f1:f5)&test_edge_left;
test(f6:f2) = test(f6:f2)&test_edge_right;

while any(test) % monotonic?
    ii = 1;
    while ii<length(Z)-1
        if ii<f1 || ii>f2 || ((ii>f5)&&(ii<f6))
            if Z(ii)>Z(ii+1)
                Z(ii) = (w(ii).*Z(ii) + w(ii+1).*Z(ii+1))./(w(ii) + w(ii+1));
                d(ii) = (w(ii).*d(ii) + w(ii+1).*d(ii+1))./(w(ii) + w(ii+1));
                w(ii) = w(ii) + w(ii+1); % important, a weighting factor for each element
                Z = [Z(1:ii);Z(ii+2:end)];
                d = [d(1:ii);d(ii+2:end)];
                w = [w(1:ii);w(ii+2:end)];
                
                edgeCenter = d(find( Z>(max(Z)+min(Z))/2, 1, 'first'));
                edgeLeft = d(find( Z<=(min(Z)+edgeMag*0.05),1,'last'));
                edgeRight = d(find( Z>=(max(Z)-edgeMag*0.05),1,'first'));
                edgeWidth = edgeRight-edgeLeft;
                cutoff1 = edgeCenter-3.5*edgeWidth; % in the unit of mm, not pixel
                cutoff2 = edgeCenter+3.5*edgeWidth;
                cutoff5 = edgeLeft;
                cutoff6 = edgeRight;
                f1 = find((d<=cutoff1),1,'last'); % in unit of pixel
                f2 = find((d>=cutoff2),1,'first')-1;
                f5 = find((d<=cutoff5),1,'last');
                f6 = find((d>=cutoff6),1,'first')-1;
                if isempty(f1)
                    f1 = 1;
                end
                if isempty(f2)
                    f2 = length(d)-1;
                end
                
            end
        else
            if Z(ii)-Z(ii+1)>edgeThreshold
                Z(ii) = (w(ii).*Z(ii) + w(ii+1).*Z(ii+1))./(w(ii) + w(ii+1));
                d(ii) = (w(ii).*d(ii) + w(ii+1).*d(ii+1))./(w(ii) + w(ii+1));
                w(ii) = w(ii) + w(ii+1); % important, a weighting factor for each element
                Z = [Z(1:ii);Z(ii+2:end)];
                d = [d(1:ii);d(ii+2:end)];
                w = [w(1:ii);w(ii+2:end)];
                
                edgeCenter = d(find( Z>(max(Z)+min(Z))/2, 1, 'first'));
                edgeLeft = d(find( Z<=(min(Z)+edgeMag*0.05),1,'last'));
                edgeRight = d(find( Z>=(max(Z)-edgeMag*0.05),1,'first'));
                edgeWidth = edgeRight-edgeLeft;
                cutoff1 = edgeCenter-3.5*edgeWidth; % in the unit of mm, not pixel
                cutoff2 = edgeCenter+3.5*edgeWidth;
                cutoff5 = edgeLeft;
                cutoff6 = edgeRight;
                f1 = find((d<=cutoff1),1,'last'); % in unit of pixel
                f2 = find((d>=cutoff2),1,'first')-1;
                f5 = find((d<=cutoff5),1,'last');
                f6 = find((d>=cutoff6),1,'first')-1;
                if isempty(f1)
                    f1 = 1;
                end
                if isempty(f2)
                    f2 = length(d)-1;
                end
            end
        end
        ii = ii + 1;
        
    end
    
    ii = length(Z)-1; % special treatment for the last elemen. redefined to avoid change of length in Z
    if Z(ii)>Z(ii+1)
        Z(ii) = (w(ii).*Z(ii) + w(ii+1).*Z(ii+1))./(w(ii) + w(ii+1));
        d(ii) = (w(ii).*d(ii) + w(ii+1).*d(ii+1))./(w(ii) + w(ii+1));
        w(ii) = w(ii) + w(ii+1);
        Z = Z(1:ii);
        d = d(1:ii);
        w = w(1:ii);
    end
    
    % reset mono rules
    edgeCenter = d(find( Z>(max(Z)+min(Z))/2, 1, 'first'));
    edgeLeft = d(find( Z<=(min(Z)+edgeMag*0.05),1,'last'));
    edgeRight = d(find( Z>=(max(Z)-edgeMag*0.05),1,'first'));
    edgeWidth = edgeRight-edgeLeft;
    cutoff1 = edgeCenter-3.5*edgeWidth; % in the unit of mm, not pixel
    cutoff2 = edgeCenter+3.5*edgeWidth;
    cutoff5 = edgeLeft;
    cutoff6 = edgeRight;
    f1 = find((d<=cutoff1),1,'last'); % in unit of pixel
    f2 = find((d>=cutoff2),1,'first')-1;
    f5 = find((d<=cutoff5),1,'last');
    f6 = find((d>=cutoff6),1,'first')-1;
    
    if isempty(f1)
        f1 = 1;
    end
    if isempty(f2)
        f2 = length(d)-1;
    end
    test = Z(1:end-1)-Z(2:end)>0; % check whether the over trend is mono
    test_edge_left = Z(f1:f5)-Z(f1+1:f5+1)>edgeThreshold; % check whether the edge is semi-mono within the cutoff
    test_edge_right = Z(f6:f2)-Z(f6+1:f2+1)>edgeThreshold; % check whether the edge is semi-mono within the cutoff
    test(f1:f5) = test(f1:f5)&test_edge_left;
    test(f6:f2) = test(f6:f2)&test_edge_right;
    
    %figure(320);plot(d_esf,esf,'b.'),hold on,plot(d,Z,'ko'),hold off
    %     pause(0.2) % pause
end
%hold on
% line([cutoff1 cutoff1],[min(Z) max(Z)]);
% line([cutoff2 cutoff2],[min(Z) max(Z)]);
% line([cutoff3 cutoff3],[min(Z) max(Z)]);
% line([cutoff4 cutoff4],[min(Z) max(Z)]);
% line([cutoff5 cutoff5],[min(Z) max(Z)]);
% line([cutoff6 cutoff6],[min(Z) max(Z)]);

% Discard the first and last points of the esf
%figure(320);plot(d_esf,esf,'b.');
%plot(d,Z,'ko');

f3 = find((d<=cutoff3),1,'last');
f4 = find((d>=cutoff4),1,'first');
if isempty(f3)
    f3 = 1;
end
if isempty(f4)
    f4 = length(d);
end
d = d(f3:f4);
Z = Z(f3:f4);

%plot(d,Z,'r*')
%hold off

% Generate final result
%d_res = linspace(min(d),max(d),128); % resample the data
d_res = d_esf;
res = zeros(size(d_res));
for i = 1:length(res)
    if d_res(i)<=min(d)
        res(i) = min(Z);
    elseif d_res(i)>=max(d)
        res(i) = max(Z);
    else
        res(i) = interp1(d,Z,d_res(i),'pchip');
        %         res(i) = interp1(d,Z,d_res(i),'linear');
    end
end

if flip  % flip res and esf back
    res = flipud(res);
    dd = diff(d_res);
    d_res = ones(size(d_res))*min(d_res);
    dd = flipud(dd);
    cs = cumsum(dd);
    d_res(2:end) = d_res(2:end)+cs;
    
    esf = flipud(esf);
    dd = diff(d_esf);
    d_esf = ones(size(d_esf))*min(d_esf);
    dd = flipud(dd);
    cs = cumsum(dd);
    d_esf(2:end) = d_esf(2:end)+cs;
    
end
end

function fig = makeNPSfigure(I,f,NPS,f_r,NPS_r,variance,ROI,xSize,ySize,fSize,name,output)

%Set nans to 0 in NPS
NPS(isnan(NPS)) = 0;

%Window and level settings for images
W=400;
L=-100;
alpha=.2;   %Transparency of ROIs

%make figure
fig=figure; 
set(fig,'units','inches','position',[0 0 xSize ySize])
set(fig,'PaperSize',[xSize ySize])
set(fig,'PaperPosition',[0 0 xSize ySize])
set(fig,'Color','k')
set(fig,'InvertHardCopy','off','Visible','on')

%make coordinate system;
x=1:size(I,1);
[X Y]=meshgrid(x,x);

%make red image
imROI=zeros(size(I,1),size(I,2),3);
imROI(:,:,1)=1;

% Make image and NPS figures
for i=1:size(ROI.NPS,1)
    
    %make legend
    leg{i}=['Section-' num2str(i) ' (' num2str(ROI.sizes(i)) ' mm)'];
    
    %show image with ROI as shaded red area
    subplot(3,size(ROI.NPS,1),i)
    im=I(:,:,ROI.NPS(i,5));
    %imshow(im,[L-W/2 L+W/2]); hold on;
    h=image(im); set(h,'CDataMapping','Scaled')
    axis image
    axis off
    hold on;
    set(gca,'Clim',[L-W/2 L+W/2])
    colormap gray
    cx=ROI.NPS(i,1);    cy=ROI.NPS(i,2);
    rmin=ROI.NPS(i,3);  rmax=ROI.NPS(i,4);
    R=sqrt((X-cx).^2 +(Y-cy).^2);
    mask= R>=rmin & R<=rmax;
    rectangle('Position',[cx-rmin,cy-rmin, 2*rmin, 2*rmin],'Curvature',[1 1],'LineStyle','-','FaceColor','none','EdgeColor','r')
    rectangle('Position',[cx-rmax,cy-rmax, 2*rmax, 2*rmax],'Curvature',[1 1],'LineStyle','-','FaceColor','none','EdgeColor','r')
    %h=imshow(imROI);
    %h=image(imROI);
    %set(h,'AlphaData',double(mask)*alpha);
    title(leg{i},'FontSize',fSize,'Color','w')
    
    %show NPS
    subplot(3,size(ROI.NPS,1),i+size(ROI.NPS,1))
    nps=NPS(:,:,i);
    %imshow(nps,[min(nps(:)) max(nps(:))],'Xdata',f,'Ydata',f);
    h= image(nps,'Xdata',f,'Ydata',f); set(h,'CDataMapping','Scaled')
    axis image
    set(gca,'Clim',[min(nps(:)) max(nps(:))])
    axis on; box on; set(gca,'XColor','w','YColor','w');
    title(['STD = ' num2str(sqrt(variance(i)),2) ' HU'],'Color','w','FontSize',fSize)
end

%Make NPS plot
subplot(3,2,5)
plot(repmat(f_r,[size(NPS_r,1) 1])',NPS_r','LineWidth',2)
set(gca,'Color','k','XColor','w','YColor','w','LineWidth',2,'FontSize',fSize)
xlabel('Spatial Frequency (1/mm)','FontSize',fSize)
ylabel('NPS (HU^2mm^2)','FontSize',fSize)
l=legend(leg);
set(l,'TextColor','w')
title('Radial NPS','FontSize',fSize,'Color','w')


%Make nNPS plot
subplot(3,2,6)
plot(repmat(f_r,[size(NPS_r,1) 1])',(NPS_r./(repmat(variance',[1 size(NPS_r,2)])))','LineWidth',2)
set(gca,'Color','k','XColor','w','YColor','w','LineWidth',2,'FontSize',fSize)
xlabel('Spatial Frequency (1/mm)','FontSize',fSize)
ylabel('nNPS (mm^2)','FontSize',fSize)
l=legend(leg);
set(l,'TextColor','w')
title('Radial NPS (normalized)','FontSize',fSize,'Color','w')

%Make Big labels
axes
set(gca,'Position',[0 0 1 1],'Visible','off');
text(.5,.975,['NPS Plots for ' name],'FontSize',18,'Color','w','HorizontalAlignment','center','Interpreter','none')
text(.025,.833,'ROI Locations','FontSize',16,'Color','w','HorizontalAlignment','center','Rotation',90)
text(.025,.52,'2D NPS','FontSize',16,'Color','w','HorizontalAlignment','center','Rotation',90)
text(.025,.22,'1D NPS','FontSize',16,'Color','w','HorizontalAlignment','center','Rotation',90)

%Save plot in output folder
print(fig, '-r300', '-dpdf',[output 'NPSplots.pdf']);
end

function fig = makeMTFfigure(I,f,MTF,ROI,xSize,ySize,fSize,name,output,contrast)

%Window and level settings for images
W=400;
L=-100;
alpha=.2;   %Transparency of ROIs

%make figure
fig=figure; 
set(fig,'units','inches','position',[0 0 xSize ySize])
set(fig,'PaperSize',[xSize ySize])
set(fig,'PaperPosition',[0 0 xSize ySize])
set(fig,'Color','k')
set(fig,'InvertHardCopy','off')

%make coordinate system;
x=1:size(I,1);
[X Y]=meshgrid(x,x);

%make red image
imROI=zeros(size(I,1),size(I,2),3);
imROI(:,:,1)=1;

% Make image and NPS figures
for i=1:size(ROI.MTF,1)
    
    %make legend
    leg{i}=['Section-' num2str(i) ' (' num2str(ROI.sizes(i)) ' mm)'];
    
    %show image with ROI as shaded red area
    subplot(2,size(ROI.MTF,1),i)
    im=I(:,:,ROI.MTF(i,5,1));
    %imshow(im,[L-W/2 L+W/2]); hold on;
    h=image(im); set(h,'CDataMapping','Scaled')
    axis image
    axis off
    hold on;
    set(gca,'Clim',[L-W/2 L+W/2])
    colormap gray
    %Make mask to show ROIs
    mask=[];
    for j=1:size(ROI.MTF,3);
        cx=ROI.MTF(i,1,j);      cy=ROI.MTF(i,2,j);
        rmin=ROI.MTF(i,3,j);    rmax=ROI.MTF(i,4,j);
        rectangle('Position',[cx-rmax,cy-rmax, 2*rmax, 2*rmax],'Curvature',[1 1],'LineStyle','-','FaceColor','none','EdgeColor','r')
        %R=sqrt((X-cx).^2 +(Y-cy).^2);
        %mask(:,:,j)= R>=rmin & R<=rmax;
    end
    %mask=logical(sum(double(mask),3));
    %h=imshow(imROI);
    %set(h,'AlphaData',double(mask(:,:,1))*alpha);
    
    title(leg{i},'FontSize',fSize,'Color','w')
    
    %show TTFs
    subplot(2,size(ROI.MTF,1),i+size(ROI.MTF,1))
    mtf=squeeze(MTF(i,:,:));
    ftemp=squeeze(f(i,:,:));
    plot(ftemp,mtf,'LineWidth',2)
    set(gca,'Color','k','XColor','w','YColor','w','LineWidth',2,'FontSize',fSize)
    if i==3
        xlabel('Spatial Frequency (1/mm)','FontSize',fSize)
    end
    if i==1
        ylabel('TTF','FontSize',fSize)
    end
    xlim([0 1.2]); ylim([0 1.2]);
    for j=1:size(contrast,2)
        leg{j}=[ROI.inserts{j} ', C: ' num2str(round(contrast(i,j))) ' HU'];
    end
    l=legend(leg,'Location','NorthOutside','FontSize',8);
    set(l,'TextColor','w')
    
    
end



%Make Big labels
axes
set(gca,'Position',[0 0 1 1],'Visible','off');
text(.5,.975,['TTF Plots for ' name],'FontSize',18,'Color','w','HorizontalAlignment','center','Interpreter','none')
text(.025,.75,'ROI Locations','FontSize',16,'Color','w','HorizontalAlignment','center','Rotation',90)
text(.025,.25,'TTFs','FontSize',16,'Color','w','HorizontalAlignment','center','Rotation',90)



%Save plot in output folder
print(fig, '-r300', '-dpdf',[output 'TTFplots.pdf']);
end

function fig = makedprimefigure(d_prime,ROI,tasks,xSize,ySize,fSize,name,output,nInsert,nTasks,model)

%make figure
fig=figure; 
set(fig,'units','inches','position',[0 0 xSize ySize])
set(fig,'PaperSize',[xSize ySize])
set(fig,'PaperPosition',[0 0 xSize ySize])
set(fig,'Color','k')
set(fig,'InvertHardCopy','off')

d_prime=squeeze(d_prime(:,nInsert,nTasks));
sizes=repmat(ROI.sizes',[1 size(d_prime,2)]);
h=plot(sizes,d_prime,'.-','LineWidth',2,'MarkerSize',12);
set(gca,'Color','k','XColor','w','YColor','w','LineWidth',2,'FontSize',fSize)
for i=1:length(nTasks)
    leg{i}=[num2str(2*tasks(nTasks(i))) ' mm'];
end
l=legend(leg); set(l,'TextColor','w');
set(gca,'XTick',ROI.sizes)
ylabel([model ' d-prime for ' ROI.inserts{nInsert} ' insert'],'FontSize',fSize)
xlabel('Phantom Size (mm)','FontSize',fSize)
title(name,'FontSize',fSize+6,'Color','w','Interpreter','none')

% % Make image and NPS figures
% for i=1:size(d_prime,3)
%     
%     %show image with ROI as shaded red area
%     subplot(size(d_prime,3),1,i)
%     h=plot(repmat(ROI.sizes',[1 size(d_prime,2)]),d_prime(:,:,i),'.-','LineWidth',2,'MarkerSize',12);
%     set(gca,'Color','k','XColor','w','YColor','w','LineWidth',2,'FontSize',fSize)
%     l=legend(ROI.inserts); set(l,'TextColor','w');
%     xlabel('Phantom Size (mm)','FontSize',fSize)
%     ylabel('d-prime','FontSize',fSize)
%     set(gca,'XTick',ROI.sizes)
%     title(['Task: Rmax = ' num2str(tasks(i,1)) ' mm, n = '  num2str(tasks(i,2)) ', C = ' num2str(tasks(i,3)) ' HU'],'FontSize',fSize,'Color','w')
%     
% end

%Make Big labels
% axes
% set(gca,'Position',[0 0 1 1],'Visible','off');
% text(.5,.975,['d-prime Plots for ' name],'FontSize',16,'Color','w','HorizontalAlignment','center','Interpreter','none')

%Save plot in output folder
print(fig, '-r300', '-dpdf',[output 'dprimeplots.pdf']);
end

function [d MTF fr df W] = getDprime2d(fMTF,MTF,fNPS,NPS,task,FOV,display,distance,ObserverType)

%Create 2D MTF
[U V]=meshgrid(fNPS,fNPS);
fr=sqrt(U.^2+V.^2);
MTF=interp1(fMTF,MTF,fr,'linear');

%Create task function
W = task(1)*(sqrt(3*task(2))./(4.*fr)).*besselj(1,2*pi*task(2).*fr);

%Get visual response function
switch ObserverType
    case 'NPW'
        E=ones(size(NPS));
    case 'NPWE'
        E=getVisualResponseFunction(fr,FOV,display,distance);
    otherwise
        warning([ObserverType ' is not a valid ''ObserverType''. Using ''NPW'' observer'])
        E=ones(size(NPS));
end

%calculate d'
df=abs(fNPS(2)-fNPS(1));
num=(sum(sum(   (W.^2).*(MTF.^2).*(E.^2)  )).*df*df).^2;
den=(sum(sum(   (W.^2).*(MTF.^2).*(E.^4).*(NPS)  )).*df*df);
d=sqrt(num./den);
end

function [HVR] = getVisualResponseFunction(f,FOV,display,distance)

a1=1.5;
a2=.98;
a3=0.68;

rho=(f*FOV*distance*pi)/(display*180);

HVR=((rho.^a1).*exp(-a2*(rho.^a3))).^2;
HVR=((rho.^a1).*exp(-a2.*(rho.^a3))).^2;
HVR=HVR/max(HVR(:));

end