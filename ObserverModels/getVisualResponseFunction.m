% Copyright 20xx - 2019. Duke University
function [HVR] = getVisualResponseFunction(f,FOV,display,distance,varargin)

if isempty(varargin)
    mode = 'Eckstein'; %Default
else
    mode = varargin{1}; 
end

a1=1.5;
switch mode
    case 'Eckstein' % Eckstein, M., Bartroff, J., Abbey, C., Whiting, J., & Bochud, F. (2003). Automated computer evaluation and optimization of image compression of x-ray coronary angiograms for signal known exactly detection tasks. Opt Express, 11(5), 460?475. Journal Article. http://doi.org/71440 [pii]
        a2=.98;
    otherwise
        warning('Chosen display function mode is not valid, use ''Saunders'' or ''Eckstein'', defaulting to ''Saunders''');
        a2=0.98;
end
a3=0.68;

rho=(f*FOV*distance*pi)/(display*180);

HVR=((rho.^a1).*exp(-a2.*(rho.^a3)));

end