% Copyright 20xx - 2019. Duke University
function model = fitDetectionAccuracyVsDoseVsContrast(x,y,z)
%x is dose
%y is contrast
%z is detection accuracy

% Set up fittype and options.
ft = fittype( '.5*(1+erf(a* (sqrt(x)*y)^b ))', 'independent', {'x', 'y'}, 'dependent', 'z' );
model=fit([x y],z,ft,'StartPoint',[.15 1]);
