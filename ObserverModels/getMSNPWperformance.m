% Copyright 20xx - 2019. Duke University
function results = getMSNPWperformance(Is,Ia,varargin)
%This function gets the observer performance from a multi-slice
%non-prewhitening matched filter observer

%Get the input parameters
[mode, nTrain, withVariability, nBoot, bootType, mask] = parseInputs(varargin,Is,Ia);

%Get sizes of images and number of repeats
rows= size(Is,1); cols = size(Is,2); slices=size(Is,3); reps_s = size(Is,4); reps_a=size(Ia,4);

% %mask the images
% Is(repmat(~mask,[1 1 1 reps_s]))=0;
% Ia(repmat(~mask,[1 1 1 reps_a]))=0;

%Vectorize the images
Npix=rows*cols;
Gs=permute(Is,[1 2 4 3]); Gs=reshape(Gs,[Npix reps_s slices]);
Ga=permute(Ia,[1 2 4 3]); Ga=reshape(Ga,[Npix reps_a slices]);
%Gs and Ga represent the vectorized images. [Npix X Nreps X Nslices]

%move all data to first dimension (only for mode = '3D')
switch mode
    case '3D'
        Gs=permute(Gs,[1 3 2]); Gs=reshape(Gs,[Npix*slices reps_s]);
        Ga=permute(Ga,[1 3 2]); Ga=reshape(Ga,[Npix*slices reps_a]);
        %Now Gs and Ga are [Npix*slices X Nreps X 1] arrays
        mask=mask(:);
        slices=1;
    otherwise
        mask = reshape(mask,[Npix 1 slices]);
end
mask=~mask;

%Get the performance
if withVariability
    %Set up bootstrap function 
    bootfun = @(x) getPerformance(x,Gs,Ga,mode,nTrain,slices,mask);
    options =  statset;
    [CI, dprime] = bootci(nBoot,{bootfun,(1:size(Gs,2))'},'type',bootType,'Options',options);
    STD = std(dprime);
    dprime= mean(dprime);
else
    dprime = getPerformance([],Gs,Ga,mode,nTrain,slices,mask);
    STD=[];
    CI = [];
end

results.dprime=dprime;
results.STD =STD;
results.CI = CI;
results.mode=mode;
results.nTrain=nTrain;
results.withVariability=withVariability;
results.nBoot=nBoot;
results.bootType=bootType;

end

function dprime = getPerformance(~,Gs,Ga,mode,nTrain,slices,mask)
%randomize order of Gs
r = random('unif',0,1,[size(Gs,2) 1]);
[~,ind]=sort(r);
Gs=Gs(:,ind,:); 

%randomize order of Ga
r = random('unif',0,1,[size(Ga,2) 1]);
[~,ind]=sort(r);
Ga=Ga(:,ind,:);

%Get the reps that are used for training model (others will be used for
%testing)
Gs_t = Gs(:,1:nTrain(1),:);
Ga_t = Ga(:,1:nTrain(2),:);

%Get the mean values for each slice
Gs_t_bar=mean(Gs_t,2); Gs_t_bar(mask)=0;%[nPix x 1 x Nslices]
Ga_t_bar=mean(Ga_t,2); Ga_t_bar(mask)=0;

%Get the template
W=Gs_t_bar-Ga_t_bar; %Get the mean difference for each slice
switch mode
    case 'SingleSlice'
        %compute the signal power for each slice
        [~,i]=max(sum(W.^2,1)); %i is the slice with the most signal power
        W=W(:,:,i); %now W is [Npix X 1 X 1]
        W=repmat(W,[1 1 slices]); %now W is [Npix X 1 X Nslices]
end
W=squeeze(W)'; %now W is [Nslices Npix]

%Apply template to each slice of training set
for i=1:slices
    w=W(i,:);
    lambda_s(i,:) = w*Gs_t(:,:,i);
    lambda_a(i,:) = w*Ga_t(:,:,i);
end
%lambda_s and lambda_a are [Nslices x nReps]

%Get the accross slice template
lambda_s_bar = mean(lambda_s,2);
lambda_a_bar = mean(lambda_a,2);
Ws=lambda_s_bar-lambda_a_bar; %Ws is a [Nslices x 1]
Ws=Ws'; %Now Ws is a [1XNslices] 

%Get the testing set
Gs_p = Gs(:,nTrain(1)+1:end,:);
Ga_p = Ga(:,nTrain(2)+1:end,:);

%Apply template to testing set
clear lambda_s lambda_a
for i=1:slices
    w=W(i,:);
    lambda_s(i,:) = w*Gs_p(:,:,i);
    lambda_a(i,:) = w*Ga_p(:,:,i);
end

%apply accross slice template to testing set to get distributions of final test
%statistic
lambda_s = Ws*lambda_s;
lambda_a = Ws*lambda_a;

%Compute performance
mu = (mean(lambda_s)- mean(lambda_a)).^2;
variance = (var(lambda_s)+var(lambda_a))/2;
dprime = sqrt(mu./variance);

end

function [mode, nTrain, withVariability, nBoot, bootType, mask] = parseInputs(args,Is,Ia)
mode = 'MultiSlice'; %'MultiSlice' (use a different template per slice), 'SingleSlice' (use the slice with the most sigal as template for all slices), or '3D' (observers sees all data at once)
nTrain(1) = round(size(Is,4)/2);   %Number of training samples
nTrain(2) = round(size(Ia,4)/2);
withVariability = true; 
nBoot = 500;
bootType = 'bca';
mask= true(size(Is(:,:,:,1)));

if length(args)==1
    mask = args{1};
elseif length(args)>1
    for i=1:2:length(args)
        switch args{i}
            case 'mode'
                mode = args{i+1};
            case 'nTrain'
                nTrain=args{i+1};
            case 'withVariability'
                withVariability=args{i+1};
            case 'nBoot'
                nBoot=args{i+1};
            case 'bootType'
                bootType=args{i+1};
            case 'mask'
                mask=args{i+1};
            otherwise
                disp(['Did not recognize setting: ' args{i}])
        end
    end
        
end

end