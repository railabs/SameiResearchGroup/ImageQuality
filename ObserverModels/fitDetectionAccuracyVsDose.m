% Copyright 20xx - 2019. Duke University
function model = fitDetectionAccuracyVsDose(x,y,mode)
%This function fits a curve of detection accuracy vs dose of the following
%form: y=.5*(1+erf(a*(x.^b)/2)) when mode = 'noOffset' or
%y=.5*(1+erf(a*((x).^b)/2)+c) when mode = 'offset', where y is detection
%accuracy, x is dose, a and b are fitting constants. This function finds
%the best fitting a and b (and c) in the least squares sense. For the
%noOffset mode, the function is linearizeable and thus has a simple
%analytical best fit. When the offset is include, the fit is done using a
%non-linear optimization routine.

switch mode
    
    case 'noOffset' %y=.5*(1+erf(a*(x.^b)/2))
        %Linearize data
        x = log(x);
        y = log(2*erfinv(2*y-1));
        
        %perform linear fit
        p=polyfit(x,y,1);
        
        %Return the model as a function handle
        b=p(1);
        a=exp(p(2));
        
        ft = fittype('.5*(1+erf(a*(x.^b)/2))','independent', {'x'});
        model = cfit(ft,a,b);
        
        
        
        
        
    case 'offset' %y=.5*(1+erf(a*((x).^b)/2)+c)
        ft=fittype('.5*(1+erf(a*((x).^b)/2)+c)');
        %options = fitoptions('StartPoint',[.5 .5 0]);
        model=fit(x,y,ft,'StartPoint',[.3 .3 -.1]);
end

end