% Copyright 20xx - 2019. Duke University
function d = getCHOdPrime(I,masks,masksn,pos)
%masks are for the signal present images
%masksn are for the signal absent images

%Set up constants
PB = [  1/128 1/64;...  %Passbands
        1/64 1/32;...
        1/32 1/16;...
        1/16 1/8;...
        1/8 1/4;...
        1/4 1/2];   
theta = [0 2*pi/5 4*pi/5 6*pi/5 8*pi/5];    %Orientations
beta = [0 pi/2];    %Phases

%Reshape I to combine slice/repeat dimensions
I = reshape(I,[size(I,1) size(I,2) size(I,3)*size(I,4)]);

for i=1:size(masks,3)
    mask = masks(:,:,i);
    maskn = masksn(:,:,i);
    
    %Crop the image and masks down to size
    [y, x]=ind2sub(size(mask),find(mask));
    mask=mask(min(y):max(y),min(x):max(x));
    im = I(min(y):max(y),min(x):max(x),:);
    
    [y, x]=ind2sub(size(maskn),find(maskn));
    maskn=maskn(min(y):max(y),min(x):max(x));
    imn = I(min(y):max(y),min(x):max(x),:);
    
    %Check to make sure sizes are equal for signal present and signal
    %absent images
    if ~isequal(size(im),size(imn))
        warning('Sizes of signal present and signal absent ROIs not equal')
    end
    
    %Create the Gabor filters
    nx = size(im,2);
    ny = size(im,1);
    [U, ~, ~, ~, ~] = getGaborFilters(PB,theta,beta,pos(i,:),nx,ny); %U is npixels x nchannels in size
    
    %Use g = U'*im(:) to produce the channelized output of the image. g is
    %nchannels x 1 
    
    %Need to produce channel output for each image realization
    clear gs; clear gb;
    for j=1:size(im,3)
        
        sig = im(:,:,j);
        sig(mask) = 0;
        bkg = imn(:,:,j);
        bkg(maskn) = 0;
        
        gs(:,j) = U'*sig(:);
        gb(:,j) = U'*bkg(:);
        
    end
    
    %compute channel means (accross the repeated images)
    gs_bar = mean(gs,2);
    gb_bar = mean(gb,2);
    
    %compute the covariance
    clear Ks; clear Kb;
    for m = 1:length(gs_bar)
        for n = 1:length(gs_bar)
           Ks(m,n) = mean( (gs(m,:)-gs_bar(m)).*(gs(n,:)-gs_bar(n))  )  ;
           Kb(m,n) = mean( (gb(m,:)-gb_bar(m)).*(gb(n,:)-gb_bar(n))  )  ;
            
        end
    end
    
    %Compute interclass scatter matrix
    S = (Ks + Kb)/2;
    
    %Invert the interclass scatter matrix
    Sinv = inv(S);
    
    %Create the hotelling template
    w = inv(S)*(gs_bar-gb_bar);
    
    %compute the test statistic for each image realization
    
end




end


function [U Ws Fc Theta Beta] = getGaborFilters(PB,theta,beta,pos,nx,ny) 
%This function creates a stack of 2D Gabor Filters (1 filter for each
%combination of the input parameters).
%
%INPUTS
%   PB      =       Passband [Low High]
%   theta   =       Orientation angle (in radians)
%   beta    =       Phase (in radians)
%   pos     =       Position [x0 y0] (in pixels) of the filter center
%   nx      =       Number of pixels in x direction
%   ny      =       Number of pixels in y direction
%OUTPUTS
%   U       =       The gabor filters in matrix form. size(U) = [npix^2 nPB*ntheta*nbeta]
%   Ws      =       Channel widths (in pixels). size(Ws) = [nPB*ntheta*nbeta 1]
%   Fc      =       Channel center frequencies. size(Fc) = [nPB*ntheta*nbeta 1]
%   Theta   =       Orientations. size(Theta) = [nPB*ntheta*nbeta 1]
%   Beta    =       Phases. size(Phases) = [nPB*ntheta*nbeta 1]
%
% Note that the output, U, is given in matrix form such that each column of
% U corresponds to a channel, and each Row corresponds to an image pixel.
% This allows you to easily compute the channelized output g_c of an input
% image, g, with g_c=U'*g; Here g is the image in vector form. If you have
% the image, G in matrix form, you can convert it easily to vector form by g=G(:); 

fc=mean(PB,2);
wf=PB(:,2)-PB(:,1);
ws=4*log(2)./(pi*wf);


[Ws,Theta, Beta]=meshgrid(ws,theta,beta);
[Fc,Theta, Beta]=meshgrid(fc,theta,beta);
Ws=Ws(:)'; Fc=Fc(:)'; Theta=Theta(:)'; Beta=Beta(:)';

x=linspace(-nx/2,nx/2,nx);
y=linspace(-ny/2,ny/2,ny);
[X Y]=meshgrid(x,y);
X=X(:); Y=Y(:);

for i=1:size(Ws,2)
    U(:,i)=calculateGaborEquation(X,Y,pos(1),pos(2),Ws(i),Fc(i),Theta(i),Beta(i));
end
end

function U = calculateGaborEquation(X,Y,x0,y0,ws,fc,theta,beta)

U=exp( -4.*log(2) .* ( (X-x0).^2 + (Y-y0).^2  )./(ws.^2) ) .* ...
    cos( 2.*pi.*fc.* ( (X-x0).*cos(theta) + (Y-y0).*sin(theta) ) + beta );
end