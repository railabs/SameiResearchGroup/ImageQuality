% Copyright 20xx - 2019. Duke University
function [U Ws Fc Theta Beta] = getGaborFilters(PB,theta,beta,pos,nx,ny) 
%This function creates a stack of 2D Gabor Filters (1 filter for each
%combination of the input parameters).
%
%INPUTS
%   PB      =       Passband [Low High]
%   theta   =       Orientation angle (in radians)
%   beta    =       Phase (in radians)
%   pos     =       Position [x0 y0] (in pixels) of the filter center
%   nx      =       Number of pixels in x direction
%   ny      =       Number of pixels in y direction
%OUTPUTS
%   U       =       The gabor filters in matrix form. size(U) = [npix^2 nPB*ntheta*nbeta]
%   Ws      =       Channel widths (in pixels). size(Ws) = [nPB*ntheta*nbeta 1]
%   Fc      =       Channel center frequencies. size(Fc) = [nPB*ntheta*nbeta 1]
%   Theta   =       Orientations. size(Theta) = [nPB*ntheta*nbeta 1]
%   Beta    =       Phases. size(Phases) = [nPB*ntheta*nbeta 1]
%
% Note that the output, U, is given in matrix form such that each column of
% U corresponds to a channel, and each Row corresponds to an image pixel.
% This allows you to easily compute the channelized output g_c of an input
% image, g, with g_c=U'*g; Here g is the image in vector form. If you have
% the image, G in matrix form, you can convert it easily to vector form by g=G(:); 

fc=mean(PB,2);
wf=PB(:,2)-PB(:,1);
ws=4*log(2)./(pi*wf);


[Ws,Theta, Beta]=meshgrid(ws,theta,beta);
[Fc,Theta, Beta]=meshgrid(fc,theta,beta);
Ws=Ws(:)'; Fc=Fc(:)'; Theta=Theta(:)'; Beta=Beta(:)';

x=linspace(-nx/2,nx/2,nx);
y=linspace(-ny/2,ny/2,ny);
[X Y]=meshgrid(x,y);
X=X(:); Y=Y(:);

for i=1:size(Ws,2)
    U(:,i)=calculateGaborEquation(X,Y,pos(1),pos(2),Ws(i),Fc(i),Theta(i),Beta(i));
end


function U = calculateGaborEquation(X,Y,x0,y0,ws,fc,theta,beta)

U=exp( -4.*log(2) .* ( (X-x0).^2 + (Y-y0).^2  )./(ws.^2) ) .* ...
    cos( 2.*pi.*fc.* ( (X-x0).*cos(theta) + (Y-y0).*sin(theta) ) + beta );