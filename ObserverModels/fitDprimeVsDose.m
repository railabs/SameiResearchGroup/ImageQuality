% Copyright 20xx - 2019. Duke University
function [model, gof] = fitDprimeVsDose(x,y)

ft=fittype('a*x^b');
[model, gof]=fit(x,y,ft,'StartPoint',[1 .5]);
