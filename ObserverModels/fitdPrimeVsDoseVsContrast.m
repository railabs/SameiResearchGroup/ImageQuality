% Copyright 20xx - 2019. Duke University
function varargout = fitdPrimeVsDoseVsContrast(x,y,z)
%x is dose
%y is contrast
%z is dprime

% Set up fittype and options.
ft = fittype( 'a*((x*y^2)^b)', 'independent', {'x', 'y'}, 'dependent', 'z' );
[model, gof] =fit([x y],z,ft,'StartPoint',[.1 1]);

switch nargout
    case 1
        varargout{1} = model;
    case 2
        varargout{1} = model;
        varargout{2} = gof;
end
