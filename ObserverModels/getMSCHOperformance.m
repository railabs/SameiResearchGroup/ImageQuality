% Copyright 20xx - 2019. Duke University
function results = getMSCHOperformance(Is,Ia,varargin)
%This function gets the observer performance from a multi-slice channelized
%hotelling observer. Is and Ia are the signal-present and signal-absent
%ROIs (Nrows x Ncolumns x Nslices x Nrepeats). 
%Based on:
%A. Ba, M.P. Eckstein, D. Racine, J.G. Ott, F. Verdun, S. Kobbe-Schmidt,
%and F.O. Bochud, "Anthropomorphic model observer performance in
%three-dimensional detection task for low-contrast computed tomography." J
%Med Imaging (Bellingham) 3, 011009 (2016).
%Written by Justin Solomon, justin.solomon@duke.edu

%Get the input parameters
[Nc, channelType, mode, nTrain, withVariability, nBoot, bootType, mask] = parseInputs(varargin,Is,Ia);

%Get the filter bank (npix X Nc matrix) 
switch channelType
    case 'DDOG'
        W = getDDOGFilters(size(Is,2),size(Is,1),Nc);
    otherwise
        disp([channelType ' ChannelType not supported, using DDOG'])
        W = getDDOGFilters(size(Is,2),size(Is,1),Nc);
end
W = W'; %Transpose so I don't always have to do it when channelizing

%Get the number of inplane pixels
rows= size(Is,1); cols = size(Is,2); reps_s = size(Is,4); reps_a=size(Ia,4);
nPix = rows*cols;

%Channelize images
for i=1:size(Is,3)
    im_s = Is(:,:,i,:); %Get the slice of interest for signal present image
    im_a = Ia(:,:,i,:); %Get the slice of interest for signal absent image
    %Reshape to be nPix x nReps
    im_s = reshape(im_s,[nPix reps_s]);
    im_a = reshape(im_a,[nPix reps_a]);
    %Channelize
    Gs(:,:,i) = W*im_s;
    Ga(:,:,i) = W*im_a;
    
    %mask the mean image this reduces effect of background differences
    %having large impact on observer score
    mask_slice = mask(:,:,i); mask_slice=mask_slice(:);
    im_s = mean(im_s,2); im_s(~mask_slice)=0;
    im_a = mean(im_a,2); im_a(~mask_slice)=0;
    Gs_bar(:,i)=W*im_s;
    Ga_bar(:,i)=W*im_a;
end
%Gs and Ga represent the channelized images. [Nc X Nreps X Nslices]
%Gs_bar and Ga_bar are the mask and averaged channelized images [Nc x
%Nslices]

%Get the performance
if withVariability
    %Set up bootstrap function 
    bootfun = @(x) getPerformance(x,Gs,Ga,Gs_bar,Ga_bar,mode,nTrain);
     options =  statset;
    [CI, dprime] = bootci(nBoot,{bootfun,(1:size(Gs,2))'},'type',bootType,'Options',options);
    STD = std(dprime);
    dprime= mean(dprime);
else
    dprime = getPerformance([],Gs,Ga,Gs_bar,Ga_bar,mode,nTrain);
    STD=[];
    CI = [];
end

results.dprime=dprime; %detectability index
results.STD =STD;      %STD of detectability index
results.CI = CI;       %95% confidence interval of detectability index
results.Nc=Nc;         %number of channels
results.channelType=channelType; %type of channels (currently only DDOG supported)
results.mode=mode;      %mode used to train the observer (single slice or multi-slice)
results.nTrain=nTrain;  %number of samples used for training
results.withVariability=withVariability; %boolean specifying if variability was estimated
results.nBoot=nBoot;        %number of bootstrap samples for estimating variability
results.bootType=bootType;  %type of bootstrap (see MATLAB's bootci function).
end

function dprime = getPerformance(~,Gs,Ga,Gs_bar,Ga_bar,mode,nTrain)

%randomize order of Gs
r = random('unif',0,1,[size(Gs,2) 1]);
[~,ind]=sort(r);
Gs=Gs(:,ind,:); 

%randomize order of Ga
r = random('unif',0,1,[size(Ga,2) 1]);
[~,ind]=sort(r);
Ga=Ga(:,ind,:);

%Get the CHO templates (only using nTrain number of image realizations to get
%templates, other realizations will be used for testing
Gs_t = Gs(:,1:nTrain(1),:);
Ga_t = Ga(:,1:nTrain(2),:);

slices=size(Gs_t,3);
switch mode
    case 'MultiSlice' %Need to get template for each slice
       for i=1:slices
           %Get covariance
           ks = cov(Gs_t(:,:,i)'); %Covariance of training data for this slice (signal-present case)
           ka = cov(Ga_t(:,:,i)'); %Covariance of training data for this slice (signal-absent case)
           Sc = (ks+ka)/2; %average covariance (i.e, iterclass scatter matrix)
           Sc_inv = inv(Sc); %inverse of covariance
           
           %get avearge response
           gs_bar = Gs_bar(:,i);
           ga_bar = Ga_bar(:,i);
           
           %compute template
           Wc(:,i) = Sc_inv*(gs_bar-ga_bar);
       end
    case 'SingleSlice' %uses the slice with the most signal power as the template
        [~,i] = max(sum((Gs_bar-Ga_bar).^2,1));
        %Get covariance
        ks = cov(Gs_t(:,:,i)'); %Covariance of training data for this slice (signal-present case)
        ka = cov(Ga_t(:,:,i)'); %Covariance of training data for this slice (signal-absent case)
        Sc = (ks+ka)/2; %average covariance (i.e, iterclass scatter matrix)
        Sc_inv = inv(Sc); %inverse of covariance
        
        %get avearge response
        gs_bar = Gs_bar(:,i);
        ga_bar = Ga_bar(:,i);
        
        %compute template
        Wc = Sc_inv*(gs_bar-ga_bar);
        %copy the template for all slices
        Wc = repmat(Wc,[1 slices]);
        
end
Wc = Wc';
%Wc is a nSlices x nChannels matrix of templates for each slice

%apply CHO templates to training set
for i=1:slices
    w = Wc(i,:);
    lambda_s(i,:) = w*Gs_t(:,:,i);
    lambda_a(i,:) = w*Ga_t(:,:,i);
end

%Get the across-slice template (Hotelling observer)
ks = cov(lambda_s');
ka = cov(lambda_a');
Sc = (ks+ka)/2;
Sc_inv = inv(Sc);
lambda_s_bar = mean(lambda_s,2);
lambda_a_bar = mean(lambda_a,2);
Who = Sc_inv*(lambda_s_bar-lambda_a_bar);
Who=Who'; %Who is now 1xnSlices.

%Get channelized data for testing set
Gs_p = Gs(:,nTrain(1)+1:end,:);
Ga_p = Ga(:,nTrain(2)+1:end,:);

%apply CHO templates to testing set
clear lambda_s lambda_a
for i=1:slices
    w = Wc(i,:);
    lambda_s(i,:) = w*Gs_p(:,:,i);
    lambda_a(i,:) = w*Ga_p(:,:,i);
end

%apply HO template to testing set to get distributions of final test
%statistic
lambdaHO_s = Who*lambda_s;
lambdaHO_a = Who*lambda_a;

%Compute performance
mu = (mean(lambdaHO_s)- mean(lambdaHO_a)).^2;
variance = (var(lambdaHO_s)+var(lambdaHO_a))/2;
dprime = sqrt(mu./variance);



end

function [Nc, channelType, mode, nTrain, withVariability, nBoot, bootType, mask] = parseInputs(args,Is,Ia)
Nc = 10;    %Number of channels
channelType = 'DDOG'; %'DDOG' or 'Gabor'
mode = 'MultiSlice'; %'MultiSlice' or 'SingleSlice'
nTrain(1) = round(size(Is,4)/2);   %Number of training samples
nTrain(2) = round(size(Ia,4)/2);
withVariability = true; 
nBoot = 500;
bootType = 'bca';
mask= true(size(Is(:,:,:,1)));

if length(args)==1
    mask = args{1};
elseif length(args)>1
    for i=1:2:length(args)
        switch args{i}
            case 'Nc'
                Nc=args{i+1};
            case 'channelType'
                channelType=args{i+1};
            case 'mode'
                mode = args{i+1};
            case 'nTrain'
                nTrain=args{i+1};
            case 'withVariability'
                withVariability=args{i+1};
            case 'nBoot'
                nBoot=args{i+1};
            case 'bootType'
                bootType=args{i+1};
            case 'mask'
                mask=args{i+1};
            otherwise
                disp(['Did not recognize setting: ' args{i}])
        end
    end
        
end

end

