% Copyright 20xx - 2019. Duke University
function link = getBinomialGeneralRegressionLink()

link.Link= @(mu) Link(mu);
link.Derivative = @(mu) Derivative(mu);
link.Inverse = @(x) .5*(1+erf(exp(x)./2));


end


function out = Link(mu)
mu(mu<=.5)=.5+eps;
mu(mu>=1)=1-eps;
out=log(2*erfinv(2*mu-1));
end

function out = Derivative(mu)
mu(mu<=.5)=.5+eps;
mu(mu>=1)=1-eps;
out=sqrt(pi)*exp(erfinv(2*mu-1).^2)./erfinv(2*mu-1);
end