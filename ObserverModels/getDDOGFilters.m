% Copyright 20xx - 2019. Duke University
function W = getDDOGFilters(nx,ny,Nc)
%This function returns a dense difference of Gaussian filter bank in the
%form of size(W) = [nxXny, Nc]

%nx = number of pixel columns
%ny =number of pixel rows
%Nc = number of channels

%get RHO
NY=1/2; %ie. nyquist frequenct in cycles/pixel
fx = getFFTfrequency(1,nx,'shifted');
fy = getFFTfrequency(1,ny,'shifted');
[Fx,Fy]=meshgrid(fx,fy);
[~,RHO]=cart2pol(Fx,Fy);

%set constants (See Ba et al, "Anthropomorphic model observer performance in
%3D detection task for low contrast CT", JMI, 2016)
Q = 1.66;
sig0 = .005;
alpha=1.4;

%compute DDOG filters
for j=1:Nc
    sig = sig0*(alpha.^j);
    C = exp(-(1/2)*(RHO./(Q*sig)).^2)-exp(-(1/2)*(RHO./(sig)).^2); %Filter profile in the frequency domain
    
    %Take inverse fourier transform to get filter in spatial domain
    w = abs(fftshift(ifft2(C)));
    w = w/sum(w(:)); %This normalizes the filter to sum to 1. 
    W(:,j)=w(:);
end
