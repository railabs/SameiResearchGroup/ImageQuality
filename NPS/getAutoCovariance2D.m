% Copyright 20xx - 2019. Duke University
function [C] = getAutoCovariance2D(I,mask,dmax,mode)
%This function estimates the autocovariance, C, of an ensemble of 2D
%image realizations, I, using only pixels specified by mask. This allows
%for NPS estimates using non-square ROIs.
%Output--------------------------------------------------------------------
%
%   C       -autocovariance of I. (same thing as autocorrelation of the
%   noise)
%
%Inputs--------------------------------------------------------------------
%
%   I       -MxNxK matrix. The first two dimensions are the image, the
%   third dimension is either repeats of the same image (ensemble mode), or
%   a series of image slices (slice mode).
%
%   mask    -MxN binary mask denoting the pixels to be used in estimating
%   C.
%
%   dmax    -maximum lag distance (in pixels) returned by C (final size of
%   C will be 2*dmax+1).
%
%   mode    -string denoting the type of data in I. Can be 'ensemble' (I
%   has repeated images of the same slice) or 'slice' (I has multiple
%   slices). When 'ensemble' mode is used, the mean value (mean(I,3) of I
%   is subtracted from each pixel and the returned C is 2D. When 'slice'
%   mode is used, the mean value is subtracted from each slice individually
%   using a polynomial fit and the returned C is a stack of 2D
%   autocovariances corresponding to each slice. If not specified, 'slice'
%   mode is used by default.
%
%Notes---------------------------------------------------------------------
% 1) changing dmax has no influence on speed of the algorithm. The full
% autocovariance is calculated and then the final C matrix is cropped
% according to dmax. 
% 2) This function uses MATLAB's built in xcorr2 to perform the cross
% correlations.
% 3) This function assumes wide-sense stationary noise statistics within
% the pixels of interest (as denoted by mask).
% 4) You can easily calculate the NPS from C using:
%     NPS = (psize.^2).*fftshift(abs(fft2(C))); where psize is the pixel
%     size
%
%Written by Justin Solomon,
%Version 2.0, May 14, 2014
%Code----------------------------------------------------------------------

%Set mode if not specified
if nargin<4
    mode='slice';
end

%Check the number of inputs
if nargin<3
    error('This function needs at least 3 inputs (I, mask, and dmax)')
end

%Check to make sure the mode is set correctly
if ~(strcmp(mode,'ensemble') | strcmp(mode,'slice'))
    error('Mode should be "ensemble" or "slice"')
end

%Change mode to 'slice' if only 1 image is inputed ('ensemble' mode only
%works with multiple image realizations
if size(I,3)==1
    mode='slice';
end

%Subtract mean of I to get zero-mean noise images (the way this is done
%depends on mode
switch mode
    case 'ensemble'
        I=I-repmat(mean(I,3),[1 1 size(I,3)]);
    case 'slice'
        method='poly';      %Set the subtraction method (could be poly or mean)
        x=1:size(I,2); y=1:size(I,1);
        [X Y]=meshgrid(x,y);
        STATS=regionprops(mask,'PixelIdxList');     %Get each region of mask
        %loop through images and subtract mean/polyfit from each region in
        %each slice
        for j=1:size(I,3)
            im=I(:,:,j);
            for i=1:length(STATS)
                switch method
                    case 'poly'
                        ind=STATS(i).PixelIdxList;
                        xval=X(ind); yval=Y(ind); pval=im(ind);
                        c=poly2fit(xval,yval,pval,2);
                        im(ind)=pval-(c(1)*xval.^2+...
                            c(2)*xval.*yval+c(3)*yval.^2+...
                            c(4)*xval+c(5)*yval+c(6)); %Poly subtraction method
                    case 'mean'
                        ind=STATS(i).PixelIdxList;
                        im(ind)=im(ind)-mean(im(ind));
                end
                
            end
            I(:,:,j)=im;  
        end
end


%Set pixels outside of mask to 0. (This way they don't add to the sums
%returned by xcorr2. 
I(repmat(~mask,[1 1 size(I,3)]))=0;

%Crop with bounding box containing the mask (saves compuation time)
[Y, X]=ind2sub(size(mask),find(mask));
I=I(min(Y):max(Y),min(X):max(X),:);
mask=mask(min(Y):max(Y),min(X):max(X),:);

%Take autocorrelation of mask (gives the number of pixels used for each lag
%position of the autocorrelation function)
mask=double(mask);
nPix=xcorr2(mask,mask);
zero_mask=nPix==0;

%loop through I and compute the sum autocorrelation for each realization/slice
C=zeros(size(nPix,1),size(nPix,2),size(I,3));
for i=1:size(I,3)
    C(:,:,i)=xcorr2(I(:,:,i),I(:,:,i));
end

%Divide C by the number of pixels used for each sum correlation to get the
%mean correlation.
switch mode
    case 'ensemble'
        C=mean(C,3)./nPix;
        C(zero_mask)=0;
    case 'slice'
        C=C./repmat(nPix,[1 1 size(I,3)]);
        C(repmat(zero_mask,[1 1 size(I,3)]))=0;
end

%Crop C according to dmax
C=C(size(I,1)-dmax:size(I,1)+dmax,size(I,2)-dmax:size(I,2)+dmax,:);


function p=poly2fit(x,y,z,n)
% POLY2FIT	POLY2FIT(x,y,z,n) finds the coefficients of a two-dimensional
%		polynomial formed from the data in the matrices x and y
%		that fits the data in the matrix z in a least-squares sense.
%		The coefficients are returned in the vector p, in descending
%		powers of x and y.  For example, the second order polynomial
%
%		x^2+xy+2x-3y-1
%
%		would be returned as [1 1 0 2 -3 -1]
%
%	Written by Jim Rees, Aug.12, 1991
%	Adapted by Jeff Siewerdsen

if any((size(x)~=size(y)) | size(x)~=size(z))
     error('X,Y, and Z matrices must be the same size')
end;

% First straighten out x,y, and z.
x=x(:);y=y(:);z=z(:);

n=n+1;
k=1;
A=zeros(size(x));
for i=n:-1:1,
     for j=1:i,
          A(:,k)=((x.^(i-j)).*(y.^(j-1)));
          k=k+1;
     end
end
p=(A\z).';

