% Copyright 20xx - 2019. Duke University
function [f, nps, varargout] = get2DNPS(I,ROIs,psize,varargin)
%This function measures the 2D NPS from an image

%ROIs [xmin ymin xsize ysize]

%get the inputs
[padSize,subtractMethod,fRange,fSamples] = parseInputs(varargin);

%get the number of slices
slices = size(I,3);

%create the frequency axes
fx = getFFTfrequency(psize(1),padSize);
fy = getFFTfrequency(psize(2),padSize);
[Fx,Fy]=meshgrid(fx,fy);
[~,Fr]=cart2pol(Fx,Fy);
dfx = fx(2)-fx(1);
dfy = fy(2)-fy(1);


%loop over all ROIs and slices
n=0;
for i=1:size(ROIs,1)
    ROI = ROIs(i,:);
    N = prod(psize)/prod(ROI(3:4)); %normalization factor for this ROI
    for j=1:slices
        n=n+1;
        %get the pixels
        im = I(ROI(2):ROI(2)+ROI(4)-1,ROI(1):ROI(1)+ROI(3)-1,j);
        %remove the mean
        im = subtractMean(im,subtractMethod,psize);
        
        NPS(:,:,n) = N*abs(fftshift(fft2(im,padSize,padSize))).^2;
        
        nps=NPS(:,:,n);
        variance(n,1) = (sum(nps(:))*dfx*dfy);
        
    end
end

%Average over all ROI positions
NPS = mean(NPS,3);

%compute the variance
noiseSTD = std(sqrt(variance));
variance=mean(variance);

%Get radial average
edges = linspace(fRange(1),fRange(2),fSamples);
[f, nps, rvar] = rebinData(Fr, NPS, edges, 0);
f=f'; nps=nps';

%get the peak frequency
[~,ind] = max(smooth(nps));
fpeak = f(ind);     %Peak of 1D radial NPS

%Get the average frequency from 1D radial NPS
p = nps/sum(nps);   %turn 1D nps into probability distribution
fav = sum(p.*f); 


%set the outputs
n = nargout-2;
for i=1:n
    switch i
        case 1
            varargout{i} = sqrt(variance);
        case 2
            settings.padSize = padSize;
            settings.subtractMethod = subtractMethod;
            settings.fRange = fRange;
            settings.fSamples = fSamples;
            settings.ROIs=ROIs;
            varargout{i}=settings;
        case 3
            stats.fx = fx;
            stats.fy = fy;
            stats.NPS_2D = NPS;
            stats.noise = sqrt(variance);
            stats.noiseSTD=noiseSTD; %Variability in noise measurements across ROIs
            stats.rvar = rvar; %Variance at each radial frequency bin
            stats.fpeak=fpeak;
            stats.fav = fav;
            stats.psize=psize;
            stats.nROIs = size(ROIs,1);
            stats.nSlices = slices;
            stats.ROIs = ROIs;
            varargout{i}=stats;
            
    end
end

end

function [padSize,subtractMethod,fRange,fSamples] = parseInputs(args)

padSize = 128;
subtractMethod = 'poly';
fRange = [0 2];
fSamples = 128;
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'padSize'
                padSize = args{i+1};
            case 'subtractMethod'
                subtractMethod = args{i+1};
            case 'fRange'
                fRange = args{i+1};
            case 'fSamples'
                fSamples = args{i+1};
        end
    end
end
end

function im = subtractMean(im,method,psize)
switch method
    case 'poly'
        
        FOV(1) = psize(1)*size(im,2);  FOV(2) = psize(2)*size(im,1);
        x=(0:size(im,2)-1)*psize(1) - FOV(1)/2;
        y=(0:size(im,1)-1)*psize(2) - FOV(2)/2;
        [X,Y]=meshgrid(x,y);
        P = poly2fit(X,Y,im,2); %Get the fitted values
        im = im-(P(1)*X.^2 + P(2)*X.*Y + P(3)*Y.^2 + P(4)*X + P(5)*Y + P(6)); %subtract the fitted values
        
    case 'mean'
        im = im - mean(im(:));
end

end
