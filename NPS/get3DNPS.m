% Copyright 20xx - 2019. Duke University
function [f, NPS, varargout] = get3DNPS(I,ROIs,psize,varargin)

%get the inputs
[padSize,subtractMethod] = parseInputs(varargin);

%rearrange psize and N to correspond to dimensions of I
psize = [psize(2) psize(1) psize(3)];

for i=1:size(ROIs,1)
    
    %Get the ROI volume
    Cx = ROIs(i,1);
    Cy = ROIs(i,2);
    Cz = ROIs(i,3);
    position = [Cy Cx Cz];
    N = [ROIs(i,5) ROIs(i,4) ROIs(i,6)];
    clear lims
    for j=1:length(N)
        if mod(N(j),2)
            %odd
            lims(j,:) = [position(j)-(N(j)-1)/2 position(j)+(N(j)-1)/2];
        else
            %even
            lims(j,:) = [position(j)-N(j)/2 position(j)-1+N(j)/2];
        end 
    end
    im = I(lims(1,1):lims(1,2),lims(2,1):lims(2,2),lims(3,1):lims(3,2));
    
    %subtract mean
    im = subtractMean(im,subtractMethod);
    
    %get the normalization factor
    norm = prod(psize)./prod(N);
    
    %compute NPS
    nps(:,:,:,i) = norm * abs(fftshift(fftn(im,[padSize padSize padSize]))).^2;
    
    
end

%average the NPS
NPS = mean(nps,4);

%Get the frequency axes
for i=1:length(psize)
    f{i} = getFFTfrequency(psize(i),padSize);
    df(i) = f{i}(2)-f{i}(1);
end

%compute the variance
variance = prod(df) * sum(NPS(:));

%Re-arrange f to be [x,y,z]
f={f{2},f{1},f{3}};


%set the outputs
n = nargout-2;
for i=1:n
    switch i
        case 1
            varargout{i} = sqrt(variance);
        case 2
            settings.padSize = padSize;
            settings.subtractMethod = subtractMethod;
            settings.fRange = [nan nan];
            settings.fSamples = nan;
            settings.ROIs=ROIs;
            varargout{i}=settings;
        case 3
            stats.fx = f{2};
            stats.fy = f{1};
            stats.fz = f{3};
            stats.NPS_2D = nan;
            stats.noise = sqrt(variance);
            stats.rvar = nan; %Variance at each radial frequency bin
            stats.fpeak=nan;
            stats.fav = nan;
            stats.psize=psize;
            stats.nROIs = size(ROIs,1);
            stats.nSlices = N(3);
            stats.N=N;
            stats.ROIs = ROIs;
            varargout{i}=stats;
            
    end
end


end

function [padSize,subtractMethod] = parseInputs(args)

padSize = 128;
subtractMethod = 'poly';
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'padSize'
                padSize = args{i+1};
            case 'subtractMethod'
                subtractMethod = args{i+1};
        end
    end
end
end

function I  = subtractMean(I,method)

try
    switch method
        case 'poly'
            [X,Y,Z] = meshgrid(1:size(I,2),1:size(I,1),1:size(I,3));    %create coordinate system
            indV = [X(:) Y(:) Z(:)];    %independet variables
            dV =  I(:);                 %dependent variable (i.e., pixel values)
            p = polyfitn(indV,dV,2);
            pv = polyvaln(p,indV);
            pv = reshape(pv,size(I));
            I = I-pv;
        case 'mean'
            I = I - mean(I(:));
    end
catch
    I = I - mean(I(:));
end

end
